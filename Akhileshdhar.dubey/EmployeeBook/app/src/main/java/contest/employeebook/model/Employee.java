package contest.employeebook.model;

/**
 * Created by akhileshdhar on 27/8/16.
 */
public class Employee {

    public Employee(){

    }
    String mId;
    String mName;
    String mImage;
    String mSkilles;
    String mPosition;
    String mCompany;
    String mExperience;


    public Employee(String mId, String mName, String mSkilles, String mPosition, String mCompany, String mExperience) {
        this.mId = mId;
        this.mName = mName;
        this.mSkilles = mSkilles;
        this.mPosition = mPosition;
        this.mCompany = mCompany;
        this.mExperience = mExperience;
    }


    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmSkilles() {
        return mSkilles;
    }

    public void setmSkilles(String mSkilles) {
        this.mSkilles = mSkilles;
    }

    public String getmPosition() {
        return mPosition;
    }

    public void setmPosition(String mPosition) {
        this.mPosition = mPosition;
    }


    public String getmCompany() {
        return mCompany;
    }

    public void setmCompany(String mCompany) {
        this.mCompany = mCompany;
    }

    public String getmExperience() {
        return mExperience;
    }

    public void setmExperience(String mExperience) {
        this.mExperience = mExperience;
    }
}
