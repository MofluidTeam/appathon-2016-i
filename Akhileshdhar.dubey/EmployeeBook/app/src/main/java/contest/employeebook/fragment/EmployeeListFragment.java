package contest.employeebook.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

import contest.employeebook.R;
import contest.employeebook.adapter.EmployeeAdapter;
import contest.employeebook.appdata.DataBaseHelper;
import contest.employeebook.interfaces.CustomItemClickListener;
import contest.employeebook.model.Employee;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EmployeeListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EmployeeListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeListFragment extends Fragment implements TextView.OnEditorActionListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Button btnSearch, btnLeft;
    EditText mtxt;
    View rootView;

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mGridLayoutManager;
    private EmployeeAdapter mAdapter;
    List<Employee> mAllEmpData;
    List<Employee> mAllEmpDataOriginal;
    List<Employee> mTempEmpData;

    LinearLayout ll_empty_list;
    TextView tv_text1, tv_text2;
    DataBaseHelper db;


    public EmployeeListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmployeeListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmployeeListFragment newInstance(String param1, String param2) {
        EmployeeListFragment fragment = new EmployeeListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  return inflater.inflate(R.layout.fragment_employee_list, container, false);

        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_employee_list, container, false);
            initialized();
            getViewControlls(rootView);
            setDataOnUi();
        }
        return rootView;
    }

    private void setDataOnUi() {
    }

    private void initialized() {
        db = new DataBaseHelper(getActivity());
        mAllEmpData = new ArrayList<Employee>();
        mAllEmpDataOriginal = new ArrayList<Employee>();
        mTempEmpData = new ArrayList<Employee>();
        mAllEmpData = db.getAllEmployeesDescription();
        mAllEmpDataOriginal=db.getAllEmployeesDescription();

    }


    private void getViewControlls(View rootView) {


        btnSearch = (Button) rootView.findViewById(R.id.btnSearch);
        btnLeft = (Button) rootView.findViewById(R.id.btnLeft);
        mtxt = (EditText) rootView.findViewById(R.id.edSearch);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list);
        mGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        ll_empty_list = (LinearLayout) rootView.findViewById(R.id.ll_empty_list);
        tv_text1 = (TextView) rootView.findViewById(R.id.tv_text1);
        tv_text2 = (TextView) rootView.findViewById(R.id.tv_text2);

        mtxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (0 != mtxt.getText().length()) {
                    String spnId = mtxt.getText().toString();
                    //  setSearchResult(spnId);
                    filter(spnId);
                } else {
                   setData();

                }
            }
        });
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mtxt.setText("");


              //  setData();

            }
        });
        setData();


    }

    public void setData() {
       mAllEmpDataOriginal=db.getAllEmployeesDescription();
        if (mAllEmpData.size() < 1) {
            ll_empty_list.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            HashSet<Employee> hashSet = new HashSet<Employee>();
            hashSet.addAll(mAllEmpData);
            mAllEmpData.clear();
            mAllEmpData.addAll(hashSet);

            mRecyclerView.setVisibility(View.VISIBLE);
            mAdapter = new EmployeeAdapter(getActivity(), mAllEmpData, new CustomItemClickListener() {
                @Override
                public void onItemIsClick(final View view, final int position, boolean isLongClick) {
                    //    Toast.makeText(getActivity(), "# from frgment" + position + " - " + mList[position], Toast.LENGTH_SHORT).show();
                    //  if(!mType.equalsIgnoreCase("ORDER")) {
                    //views.add(view);
                    EmployeeDetailsFragment fragment = EmployeeDetailsFragment.
                            newInstance(mAllEmpData.get(position).getmId(), " ");
                    callFragment(fragment, "Employee Details");

                }
            });

            mRecyclerView.setAdapter(mAdapter);
           // mAdapter.notifyDataSetChanged();
        }
    }


    public void filter(String charText) {
       /* HashSet<Employee> hashSet = new HashSet<Employee>();
        hashSet.addAll(mAllEmpData);
        mAllEmpData.clear();
        mAllEmpData.addAll(hashSet);
*/
        mAllEmpDataOriginal=db.getAllEmployeesDescription();
        mTempEmpData = new ArrayList<Employee>();

            mRecyclerView.setVisibility(View.VISIBLE);
            mAdapter = new EmployeeAdapter(getActivity(), mAllEmpData, new CustomItemClickListener() {
                @Override
                public void onItemIsClick(final View view, final int position, boolean isLongClick) {
                    //    Toast.makeText(getActivity(), "# from frgment" + position + " - " + mList[position], Toast.LENGTH_SHORT).show();
                    //  if(!mType.equalsIgnoreCase("ORDER")) {
                    //views.add(view);
                    EmployeeDetailsFragment fragment = EmployeeDetailsFragment.
                            newInstance(mAllEmpData.get(position).getmId(), " ");
                    callFragment(fragment, "Employee Details");

                }
            });


            charText = charText.toLowerCase(Locale.getDefault());
            mTempEmpData.clear();
            if (charText.length() == 0) {
                mTempEmpData.addAll(mAllEmpDataOriginal);
            } else {
                for (Employee emp : mAllEmpDataOriginal) {
                    if (emp.getmName().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        mTempEmpData.add(emp);
                    }
                    if (emp.getmPosition().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        mTempEmpData.add(emp);
                    }
                    if (emp.getmCompany().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        mTempEmpData.add(emp);
                    }
                    if (emp.getmSkilles().toLowerCase(Locale.getDefault())
                            .contains(charText)) {
                        mTempEmpData.add(emp);
                    }
                }
            }
           mAllEmpData.clear();
            mAllEmpData.addAll(mTempEmpData);
            //mVisibleEmpData.addAll(mTempEmpData);
            HashSet<Employee> hashSet = new HashSet<Employee>();
            hashSet.addAll(mAllEmpData);
            mAllEmpData.clear();
            mAllEmpData.addAll(hashSet);
        if (mAllEmpData.size() < 1) {
            ll_empty_list.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);

        }
            mAdapter.notifyDataSetChanged();


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @Override
    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
        return false;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void callFragment(Fragment nextFragment, String tag) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        // HomeFragment homeFragment = new HomeFragment();
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.add(R.id.home_container, nextFragment, tag);
        fragmentTransaction.commit();
    }

}
