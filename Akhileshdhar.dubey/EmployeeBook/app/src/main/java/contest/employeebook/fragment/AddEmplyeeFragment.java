package contest.employeebook.fragment;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import contest.employeebook.LauncherActivity;
import contest.employeebook.R;
import contest.employeebook.appdata.DataBaseHelper;
import contest.employeebook.model.EmployeeDetails;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddEmplyeeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AddEmplyeeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddEmplyeeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    DataBaseHelper dbHelper;
    View rootView;

    String mName;
    String mCompany;
    String mPosition;
    String mSkilles;


    String mExperience;


    String mEmployeeId;
    String mPhone;
    String mEmail;
    String mAddress;
    String mDob;
    String mObjective;
    String mAchievments;
    String mDepartmentName;

    EditText edtName,
            edtCompany,
            edtDesignation,
            edtSkills,
            edtEdperi,
            edtEmpId,
            edtPhone,
            edtEmail,
            edtAddress,
            edtage,
            edtObje,
            edtAchieve,
            edtDeport;
    Button btnAdd;


    public AddEmplyeeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddEmplyeeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddEmplyeeFragment newInstance(String param1, String param2) {
        AddEmplyeeFragment fragment = new AddEmplyeeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.fragment_add_emplyee, container, false);

        if (rootView == null) {

            rootView = inflater.inflate(R.layout.fragment_add_emplyee, container, false);
            initialized();
            getViewControlls(rootView);
            setDataOnUi();


        }
        return rootView;
    }


    private void setDataOnUi() {
    }

    private void initialized() {
        dbHelper = new DataBaseHelper(getActivity());

    }


    private void getViewControlls(View rootView) {

        edtName = (EditText) rootView.findViewById(R.id.edt_name);
        edtCompany = (EditText) rootView.findViewById(R.id.edt_comp);
        edtDesignation = (EditText) rootView.findViewById(R.id.edt_pos);
        edtSkills = (EditText) rootView.findViewById(R.id.edt_skills);
        edtEdperi = (EditText) rootView.findViewById(R.id.edt_experi);
        edtEmpId = (EditText) rootView.findViewById(R.id.edt_empid);
        edtPhone = (EditText) rootView.findViewById(R.id.edt_mobile);
        edtEmail = (EditText) rootView.findViewById(R.id.edt_email);
        edtAddress = (EditText) rootView.findViewById(R.id.edt_address);
        edtage = (EditText) rootView.findViewById(R.id.edt_age);
        edtObje = (EditText) rootView.findViewById(R.id.edt_objective);
        edtAchieve = (EditText) rootView.findViewById(R.id.edt_achievement);
        edtDeport = (EditText) rootView.findViewById(R.id.edt_depot);
        btnAdd = (Button) rootView.findViewById(R.id.btn_add);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtName.getText() == null || edtName.getText().length() < 1) {
                    edtName.setError("Can't be empty!");
                    return;
                }

                if (edtCompany.getText() == null || edtCompany.getText().length() < 1) {
                    edtCompany.setError("Can't be empty!");
                    return;
                }

                if (edtDesignation.getText() == null || edtDesignation.getText().length() < 1) {
                    edtDesignation.setError("Can't be empty!");
                    return;
                }

                if (edtSkills.getText() == null || edtSkills.getText().length() < 1) {
                    edtSkills.setError("Can't be empty!");
                    return;
                }

                if (edtEdperi.getText() == null || edtEdperi.getText().length() < 1) {
                    edtEdperi.setError("You look like fresher.");
                    //  return;
                }


                if (edtEmpId.getText() == null || edtEmpId.getText().length() < 1) {
                    edtEmpId.setError("Can't be empty!");
                    return;
                }
                if (edtPhone.getText() == null || edtPhone.getText().length() < 1) {
                    edtPhone.setError("Can't be empty!");
                    return;
                }

                if (edtEmail.getText() == null || edtEmail.getText().length() < 1) {
                    edtEmail.setError("Can't be empty!");
                    return;
                }

                if (edtAddress.getText() == null || edtAddress.getText().length() < 1) {
                    edtAddress.setError("Can't be empty!");
                    return;
                }


                if (edtage.getText() == null || edtage.getText().length() < 1) {
                    edtage.setError("Can't be empty!");
                    return;
                }

                if (edtObje.getText() == null || edtObje.getText().length() < 1) {
                    edtObje.setError("Can't be empty!");
                    return;
                }
                if (edtAchieve.getText() == null || edtAchieve.getText().length() < 1) {
                    edtAchieve.setError("You look you are not achiever");
                    //  return;
                }

                if (edtDeport.getText() == null || edtDeport.getText().length() < 1) {
                    edtDeport.setError("Can't be empty!");
                    return;
                }

                EmployeeDetails emp = new EmployeeDetails();
                emp.setmName(edtName.getText().toString());
                emp.setmCompany(edtCompany.getText().toString());
                emp.setmPosition(edtDesignation.getText().toString());
                emp.setmSkilles(edtSkills.getText().toString());
                emp.setmExperience(edtEdperi.getText().toString());
                emp.setmEmployeeId(edtEmpId.getText().toString());
                emp.setmPhone(edtPhone.getText().toString());

                emp.setmEmail(edtEmail.getText().toString());
                emp.setmAddress(edtAddress.getText().toString());
                emp.setmDob(edtage.getText().toString());
                emp.setmObjective(edtObje.getText().toString());
                emp.setmAchievments(edtAchieve.getText().toString());
                emp.setmDepartmentName(edtDeport.getText().toString());

                Long i = dbHelper.addEmployee(emp);
                if (i > 0) {
                    ((LauncherActivity) getActivity()).showSnakbar("Your record added. ", "");
                    //  NotifyEmpAdded();

                } else {
                    ((LauncherActivity) getActivity()).showSnakbar("Your record can't be add. ", "try anoter");
                }


            }
        });

       /* btnSearch = (Button)rootView.findViewById(R.id.btnSearch);
        btnLeft = (Button) rootView.findViewById(R.id.btnLeft);
        mtxt = (EditText) rootView.findViewById(R.id.edSearch);
        mRecyclerView = (RecyclerView)rootView.findViewById(R.id.list);
        mGridLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mGridLayoutManager);
*/

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    void NotifyEmpAdded() {
        Dialog diag = new Dialog(getActivity());
        diag.setTitle("Add new Employee");
        TextView txt = new TextView(getActivity());
        txt.setText("Employee Added Successfully");
        diag.setContentView(txt);
        diag.show();
        try {
            diag.wait(1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            // CatchError(e.toString());
        }
        diag.notify();
        diag.dismiss();
    }
}
