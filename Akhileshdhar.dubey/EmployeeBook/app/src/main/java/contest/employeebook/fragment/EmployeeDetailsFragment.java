package contest.employeebook.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import contest.employeebook.LauncherActivity;
import contest.employeebook.R;
import contest.employeebook.appdata.DataBaseHelper;
import contest.employeebook.model.EmployeeDetails;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EmployeeDetailsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EmployeeDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EmployeeDetailsFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    TextView btnCall, btnEmail, btnSms;

    View rootView;
    DataBaseHelper db;
    EmployeeDetails empDetails;


    public EmployeeDetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EmployeeDetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EmployeeDetailsFragment newInstance(String param1, String param2) {
        EmployeeDetailsFragment fragment = new EmployeeDetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.fragment_employee_details, container, false);
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_employee_details, container, false);
            initialized();
            getViewControlls(rootView);
            //  setDataOnUi();


        }
        return rootView;
    }


    private void initialized() {
        db = new DataBaseHelper(getActivity());
        empDetails = new EmployeeDetails();
        empDetails = db.getAllEmployeeDetails(mParam1);

    }


    private void getViewControlls(View rootView) {


        // btnSearch = (Button)rootView.findViewById(R.id.btnSearch);


        btnCall = (TextView) rootView.findViewById(R.id.btn_call);
        btnEmail = (TextView) rootView.findViewById(R.id.btn_email);
        btnSms = (TextView) rootView.findViewById(R.id.btn_sms);

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  if (isTelephonyEnabled()){
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + empDetails.getmPhone()));
                startActivity(callIntent);
       /* }else{
                ((MainActivity)getActivity()).showSnakbar("Only emergacy call allowed on this device!","");
            }*/
            }
        });
        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMail();


            }
        });
        btnSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendSMS("", "");
            }
        });


        TextView tvName, tvComp, tvPos, tvAddrs, tvAge, tvObjective, tvAchievements, tvExperi, tvSkills, tvDeportNAmne;
        tvName = (TextView) rootView.findViewById(R.id.tv_name);
        tvComp = (TextView) rootView.findViewById(R.id.tv_company);
        tvPos = (TextView) rootView.findViewById(R.id.tv_position);
        tvAddrs = (TextView) rootView.findViewById(R.id.tv_address);
        tvAge = (TextView) rootView.findViewById(R.id.tv_age);
        tvObjective = (TextView) rootView.findViewById(R.id.tv_objective);

        tvAchievements = (TextView) rootView.findViewById(R.id.tv_achievement);
        tvExperi = (TextView) rootView.findViewById(R.id.tv_exper);
        tvSkills = (TextView) rootView.findViewById(R.id.tv_skill);
        tvDeportNAmne = (TextView) rootView.findViewById(R.id.tv_deport);

        tvName.setText(empDetails.getmName());
        tvComp.setText(empDetails.getmCompany());
        tvPos.setText(empDetails.getmPosition());
        tvAddrs.setText(empDetails.getmAddress());
        tvAge.setText(empDetails.getmDob());
        tvObjective.setText(empDetails.getmObjective());
        tvAchievements.setText(empDetails.getmAchievments());
        tvExperi.setText(empDetails.getmExperience());
        tvSkills.setText(empDetails.getmSkilles());
        tvDeportNAmne.setText(empDetails.getmDepartmentName());

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void sendSMS(String phoneNumber, String message) {


        if (phoneNumber.equals("")) {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", empDetails.getmPhone(), null));
            sendIntent.putExtra("sms_body", "");
            sendIntent.setType("vnd.android-dir/mms-sms");
            sendIntent.putExtra("address", empDetails.getmPhone());
            startActivity(sendIntent);

        } else {
            String SENT = "SMS_SENT";
            String DELIVERED = "SMS_DELIVERED";

            PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0,
                    new Intent(SENT), 0);

            PendingIntent deliveredPI = PendingIntent.getBroadcast(getActivity(), 0,
                    new Intent(DELIVERED), 0);

            //---when the SMS has been sent---
            getActivity().registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            Toast.makeText(getActivity().getBaseContext(), "SMS sent",
                                    Toast.LENGTH_SHORT).show();
                            ((LauncherActivity) getActivity()).showSnakbar("SMS sent", "");
                            break;
                        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:

                            ((LauncherActivity) getActivity()).showSnakbar("Generic failure", "");
                            break;
                        case SmsManager.RESULT_ERROR_NO_SERVICE:
                            ((LauncherActivity) getActivity()).showSnakbar("No service", "");

                            break;
                        case SmsManager.RESULT_ERROR_NULL_PDU:
                            ((LauncherActivity) getActivity()).showSnakbar("Null PDU", "");

                            break;
                        case SmsManager.RESULT_ERROR_RADIO_OFF:
                            ((LauncherActivity) getActivity()).showSnakbar("Radio off", "");

                            break;
                    }
                }
            }, new IntentFilter(SENT));

            //---when the SMS has been delivered---
            getActivity().registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context arg0, Intent arg1) {
                    switch (getResultCode()) {
                        case Activity.RESULT_OK:
                            ((LauncherActivity) getActivity()).showSnakbar("SMS delivered", "");

                            break;
                        case Activity.RESULT_CANCELED:
                            ((LauncherActivity) getActivity()).showSnakbar("SMS not delivered", "");
                            break;
                    }
                }
            }, new IntentFilter(DELIVERED));
            try {
                SmsManager sms = SmsManager.getDefault();
                sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
            } catch (Exception e) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.SEND_SMS}, 1);
                SmsManager sms = SmsManager.getDefault();
                sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
            }
        }
    }

    public void sendMail() {

        String[] TO = {empDetails.getmEmail()};
        String[] CC = {""};
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, TO);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Employee Book");


        try {
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
            // Log.i("Finished sending email...", "");
        } catch (android.content.ActivityNotFoundException ex) {
            // Toast.makeText(getActivity(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
            ((LauncherActivity) getActivity()).showSnakbar("There is no email client installed.", "");
        }


    }

}
