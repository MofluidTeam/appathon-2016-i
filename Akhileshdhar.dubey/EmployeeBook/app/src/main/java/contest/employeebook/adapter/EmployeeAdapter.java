package contest.employeebook.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import contest.employeebook.interfaces.CustomItemClickListener;
import contest.employeebook.model.Employee;
import contest.employeebook.R;


/**
 * Created by akhileshdhar on 27/8/16.
 */
public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.ViewHolder> {
    private Context mContext;
    // private String[] mList;
    private  List<Employee> mValues;
    CustomItemClickListener mListener;

    public EmployeeAdapter(Context contexts, List<Employee> studentList, CustomItemClickListener listener) {
        this.mContext = contexts;
        this.mListener = listener;
        this.mValues = studentList;
    }
    public  void addItem(List<Employee> studentList){
        this.mValues = studentList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.row_item_employee_layout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mName.setText(mValues.get(position).getmName());
        holder.mPosition.setText(mValues.get(position).getmPosition());
        holder.mCompany.setText(mValues.get(position).getmCompany());
        holder.mExperienc.setText(mValues.get(position).getmExperience());
        holder.mSkills.setText(mValues.get(position).getmSkilles());


        holder.setClickListener(mListener);

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {
        public TextView mName;
        public TextView mPosition;
        public TextView mCompany;
        public TextView mExperienc;
        public TextView mSkills;


        public Employee mItem;
        private CustomItemClickListener clickListener;

        public ViewHolder(View itemView) {
            super(itemView);

            mName = (TextView) itemView.findViewById(R.id.tv_name);
            mPosition = (TextView) itemView.findViewById(R.id.tv_position);
            mCompany = (TextView) itemView.findViewById(R.id.tv_company);
            mExperienc= (TextView) itemView.findViewById(R.id.tv_experience);
            mSkills= (TextView) itemView.findViewById(R.id.tv_skill);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        public void setClickListener(CustomItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemIsClick(view, getPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onItemIsClick(view, getPosition(), false);
            return true;
        }
    }

    public static String getShortName(String name) {
        String abbriatedName = "";


        if (name.contains(" ")) {
            String[] names = name.split(" ");

            for (String s : names) {
                abbriatedName = abbriatedName + s.charAt(0);
            }

            return abbriatedName;
        } else if (name.length() > 2) {
            abbriatedName = name.substring(0, 1);

        } else {
            abbriatedName = name;
        }
        return abbriatedName;
    }
}
