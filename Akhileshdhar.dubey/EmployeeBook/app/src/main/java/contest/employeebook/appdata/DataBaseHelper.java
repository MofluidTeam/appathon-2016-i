package contest.employeebook.appdata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import java.util.ArrayList;
import java.util.List;

import contest.employeebook.model.Employee;
import contest.employeebook.model.EmployeeDetails;

/**
 * Created by akhileshdhar on 27/8/16.
 */
public class DataBaseHelper extends SQLiteOpenHelper {


 /*   String mId;
    String mName;
    String mSkilles;
    String mPosition;
    String mCompany;
    String mExperience;


    String mEmployeeId;
    String mPhone;
    String mEmail;
    String mAddress;
    String mDob;
    String mObjective;
    String mAchievments;
    String mDepartmentName;
*/

    static final String dbName = "demoDB";
    static final String employeeTable = "Employees";

    //short
    static final String colID = "ID";
    static final String colName = "EmployeeName";
    static final String colSkilles = "skills";
    static final String colPos = "Designation";
    static final String colCompName = "CompName";
    static final String colExperience = "Experience";

    //details
    static final String colempId = "EmpID";
    static final String colPhone = "Mobile";
    static final String colEmail = "Email";
    static final String colAddress = "Address";
    static final String colDOB = "Age";
    static final String colObjective = "Objective";
    static final String colAchievements = "Achievements";
    static final String colDeptName = "DeptName";

    static final String viewEmps = "ViewEmps";


    public DataBaseHelper(Context context) {
        super(context, dbName, null, 1);

        // TODO Auto-generated constructor stub
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub


        db.execSQL("CREATE TABLE " + employeeTable
                + " (" + colID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + colName + " TEXT,"
                + colempId + " TEXT, "
                + colSkilles + " TEXT,"
                + colPos + " TEXT,"
                + colCompName + " TEXT,"
                + colExperience + " TEXT,"
                + colDOB + " TEXT,"
                + colPhone + " TEXT,"
                + colEmail + " TEXT NOT NULL UNIQUE,"
                + colAddress + " TEXT,"
                + colObjective + " TEXT,"
                + colAchievements + " TEXT,"
                + colDeptName + " TEXT)");


        //   InsertDepts(db);

    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + employeeTable);
        onCreate(db);
    }

    public long addEmployee(EmployeeDetails emp) {
        long i = -1;
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(colName, emp.getmName());
        cv.put(colSkilles, emp.getmSkilles());
        cv.put(colPos, emp.getmPosition());
        cv.put(colCompName, emp.getmCompany());
        cv.put(colExperience, emp.getmExperience());

        cv.put(colempId, emp.getmEmployeeId());
        cv.put(colPhone, emp.getmPhone());
        cv.put(colEmail, emp.getmEmail());
        cv.put(colAddress, emp.getmAddress());
        cv.put(colDOB, emp.getmDob());
        cv.put(colObjective, emp.getmObjective());
        cv.put(colAchievements, emp.getmAchievments());
        cv.put(colDeptName, emp.getmDepartmentName());
        i = db.insert(employeeTable, colName, cv);
        db.close();
        return i;
    }

    int getEmployeeCount() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cur = db.rawQuery("Select * from " + employeeTable, null);
        int x = cur.getCount();
        cur.close();
        return x;
    }

    public List<Employee> getAllEmployeesDescription() {
        List<Employee> empList = new ArrayList<Employee>();
// Select All Query
        String selectQuery = "SELECT * FROM " + employeeTable;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Employee emp = new Employee();
                emp.setmId(cursor.getString(0));
                emp.setmName(cursor.getString(1));
                //  emp.setmEmployeeId(cursor.getString(2));
                emp.setmSkilles(cursor.getString(3));
                emp.setmPosition(cursor.getString(4));
                emp.setmCompany(cursor.getString(5));
                emp.setmExperience(cursor.getString(6));
                // Adding contact to list
                empList.add(emp);
            } while (cursor.moveToNext());
        }
        // return contact list
        return empList;

    }

    public EmployeeDetails getAllEmployeeDetails(String id) {
        // SQLiteDatabase db=this.getReadableDatabase();
        // Cursor cur=db.rawQuery("SELECT "+colDeptID+" as _id, "+colDeptName+" from "+deptTable,new String [] {});
        // return cur;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from " + employeeTable + " where id = ' " + id + " ' ", null);

        if (cursor != null)
            cursor.moveToFirst();
        EmployeeDetails emp = new EmployeeDetails();
        emp.setmId(cursor.getString(0));
        emp.setmName(cursor.getString(1));
        emp.setmEmployeeId(cursor.getString(2));
        emp.setmSkilles(cursor.getString(3));
        emp.setmPosition(cursor.getString(4));
        emp.setmCompany(cursor.getString(5));
        emp.setmExperience(cursor.getString(6));


        emp.setmDob(cursor.getString(7));
        emp.setmPhone(cursor.getString(8));
        emp.setmEmail(cursor.getString(9));
        emp.setmAddress(cursor.getString(10));
        emp.setmObjective(cursor.getString(11));
        emp.setmAchievments(cursor.getString(12));
        emp.setmDepartmentName(cursor.getString(13));
        // Adding contact to list

        return emp;


    }

    /*  public int UpdateEmp(Employee emp)
    {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues cv=new ContentValues();
        cv.put(colName, emp.getName());
        cv.put(colempId, emp.getempID());
        cv.put(colAge, emp.getAge());
        cv.put(colMobile, emp.getMobile());
        cv.put(colDesg, emp.getDesg());
        cv.put(colDept, emp.getDept());
        return db.update(employeeTable, cv, colID+"=?", new String []{String.valueOf(emp.getID())});
    }
  */
    public void deleteEmp(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(employeeTable, colID + "=?", new String[]{id});
        db.close();
    }
}

