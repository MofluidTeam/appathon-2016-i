package contest.employeebook.interfaces;

import android.view.View;

/**
 * Created by akhileshdhar on 27/8/16.
 */
public interface CustomItemClickListener {
    void onItemIsClick(View view, int position, boolean isLongClick);


}
