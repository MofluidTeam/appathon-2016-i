package contest.employeebook;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import contest.employeebook.fragment.AddEmplyeeFragment;
import contest.employeebook.fragment.EmployeeListFragment;

public class LauncherActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    CoordinatorLayout coordinatorLayout;
    Toolbar toolbar;
    FrameLayout fragmentContainer;


    boolean doubleBackToExitPressedOnce;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);
        fragmentContainer = (FrameLayout) findViewById(R.id.home_container);

        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG) .setAction("Action", null).show();
                AddEmplyeeFragment fragment = AddEmplyeeFragment.newInstance("arg1", "arg2");
                callFragment(fragment, "New Employee ");

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        EmployeeListFragment fragment = EmployeeListFragment.newInstance("arg1", "arg2");
        callFragment(fragment, "Employees ");

    }


  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.launcher, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_new_employee) {
            // Handle the camera action
            AddEmplyeeFragment fragment = AddEmplyeeFragment.newInstance("arg1", "arg2");
            try {

                Fragment visiBleFragment = LauncherActivity.this.getSupportFragmentManager().findFragmentById(R.id.home_container);
                if (visiBleFragment instanceof AddEmplyeeFragment) {


                } else {
                    try {

                        FragmentManager fm = getSupportFragmentManager();
                        int count = fm.getBackStackEntryCount();
                        while (count > 1) {
                            fm.popBackStackImmediate();
                            count--;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    callFragment(fragment, "New Employee ");

                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        } else if (id == R.id.nav_employees) {
            EmployeeListFragment fragment = EmployeeListFragment.newInstance("arg1", "arg2");




            try {

                Fragment visiBleFragment = LauncherActivity.this.getSupportFragmentManager().findFragmentById(R.id.home_container);
                if (visiBleFragment instanceof EmployeeListFragment) {


                } else {
                    try {

                        FragmentManager fm = getSupportFragmentManager();
                        int count = fm.getBackStackEntryCount();
                        while (count > 1) {
                            fm.popBackStackImmediate();
                            count--;
                        }


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    callFragment(fragment, "Employees");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            try {
                FragmentManager fm = getSupportFragmentManager();

                int count = fm.getBackStackEntryCount();
                Fragment visiBleFragment = LauncherActivity.this.getSupportFragmentManager().findFragmentById(R.id.home_container);

                if (visiBleFragment instanceof EmployeeListFragment) {
                    if (doubleBackToExitPressedOnce) {
                        super.onBackPressed();
                        this.finish();
                        //  return;
                    }
                    this.doubleBackToExitPressedOnce = true;
                    // Toast.makeText(this, fetchStringresource(R.string.back_to_exit), Toast.LENGTH_SHORT).show();
                    showSnakbar("Press back key again for exit.", "");

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);

                } else {
                    try {
                        popCurrentFragment();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    private void popCurrentFragment() {
        FragmentManager fm = getSupportFragmentManager();


        int count = fm.getBackStackEntryCount();
        if (count > 1) {
            fm.popBackStackImmediate();
        }
        Fragment currentFragment = fm.findFragmentById(R.id.home_container);
        String name=currentFragment.getTag();
        count = fm.getBackStackEntryCount();


    }


    public void callFragment(Fragment nextFragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        // HomeFragment homeFragment = new HomeFragment();
        fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.add(R.id.home_container, nextFragment, tag);
        fragmentTransaction.commit();

    }

    public void showSnakbar(String msg, String action) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG)
                .setAction(action, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });

        // Changing message text color
        snackbar.setActionTextColor(Color.GREEN);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setMaxLines(5);

        snackbar.show();
    }


}
