package contest.employeebook.model;

/**
 * Created by akhileshdhar on 27/8/16.
 */
public class EmployeeDetails extends  Employee {

    String mEmployeeId;
    String mPhone;
    String mEmail;
    String mAddress;
    String mDob;
    String mObjective;
    String mAchievments;
    String mDepartmentName;

    public String getmDepartmentName() {
        return mDepartmentName;
    }

    public void setmDepartmentName(String mDepartmentName) {
        this.mDepartmentName = mDepartmentName;
    }





    public String getmEmployeeId() {
        return mEmployeeId;
    }

    public void setmEmployeeId(String mEmployeeId) {
        this.mEmployeeId = mEmployeeId;
    }


    public String getmPhone() {
        return mPhone;
    }

    public void setmPhone(String mPhone) {
        this.mPhone = mPhone;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmDob() {
        return mDob;
    }

    public void setmDob(String mDob) {
        this.mDob = mDob;
    }

    public String getmObjective() {
        return mObjective;
    }

    public void setmObjective(String mObjective) {
        this.mObjective = mObjective;
    }

    public String getmAchievments() {
        return mAchievments;
    }

    public void setmAchievments(String mAchievments) {
        this.mAchievments = mAchievments;
    }
}
