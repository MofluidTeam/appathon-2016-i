//
//  mainViewCell.swift
//  SocialApp
//
//  Created by Vivek Shukla on 27/08/16.
//  Copyright © 2016 Vivek Shukla. All rights reserved.
//

import UIKit

class mainViewCell: UITableViewCell {

    @IBOutlet weak var ImgView: UIImageView!
    @IBOutlet weak var lblTopic: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
