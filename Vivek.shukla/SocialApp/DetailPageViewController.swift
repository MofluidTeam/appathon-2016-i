//
//  DetailPageViewController.swift
//  SocialApp
//
//  Created by Vivek Shukla on 27/08/16.
//  Copyright © 2016 Vivek Shukla. All rights reserved.
//

import UIKit

class DetailPageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

      self.navigationController!.navigationBar.tintColor = UIColor.whiteColor();
        let share = UIBarButtonItem(image: UIImage(named: "share-2"),style: UIBarButtonItemStyle.Plain ,target: self, action: "OnMenuClicked:")
    
    
        self.navigationItem.rightBarButtonItem =  share
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

    @IBAction func commentBtn(sender: AnyObject) {
        
        let productDetails  =  CommentViewController(nibName: "CommentViewController", bundle: NSBundle.mainBundle())
        self.navigationController?.pushViewController(productDetails, animated: true)
    }
}
