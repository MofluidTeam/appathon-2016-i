//
//  ProfileThoughtViewCell.swift
//  SocialApp
//
//  Created by Vivek Shukla on 27/08/16.
//  Copyright © 2016 Vivek Shukla. All rights reserved.
//

import UIKit

class ProfileThoughtViewCell: UITableViewCell {


    @IBOutlet weak var lblthought: UILabel!
    @IBOutlet weak var btnLink: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var ImgPhoto: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
