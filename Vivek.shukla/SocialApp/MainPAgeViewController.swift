//
//  MainPAgeViewController.swift
//  SocialApp
//
//  Created by Vivek Shukla on 27/08/16.
//  Copyright © 2016 Vivek Shukla. All rights reserved.
//

import UIKit

class MainPAgeViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    var names = ["Mehbooba meets PM Modi, slams Pak for fuelling protests in Kashmir", "50th day of curfew in Kashmir: 10 developments", "Argentina: General Luciano Menendez and 27 others sentenced to life for dictatorship crimes", "Govt set to slap terror case on Zakir Naik", "Siniya Sonamaylya","Joan D. Vinge", "Sonia Sotomayor", "Floods wreak havoc: 1,000 lives lost, Rs 11,000cr drowned", "Beauty queens with fabulous fashion statement", "Beauty queens with fabulous fashion statement"]
    
    var images = [UIImage(named: "NEWS1"),UIImage(named: "NEWS2"),UIImage(named: "NEWS3"),UIImage(named: "NEWS4"),UIImage(named: "NEWS5"),UIImage(named: "NEWS6"),UIImage(named: "image")]

    override func viewDidLoad() {
        super.viewDidLoad()
        
          self.navigationController!.navigationBar.tintColor = UIColor.whiteColor();


        
        let search = UIBarButtonItem(image: UIImage(named: "search"),style: UIBarButtonItemStyle.Plain ,target: self, action: "OnMenuClicked:")
        
         let menu = UIBarButtonItem(image: UIImage(named: "menu.png"),style: UIBarButtonItemStyle.Plain ,target: self, action: "OnMenuClicked:")
        
        self.navigationItem.leftBarButtonItem = menu
        self.navigationItem.rightBarButtonItem = search
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCellWithIdentifier("cell",  forIndexPath: indexPath) as? mainViewCell
        
        cell!.ImgView.image = images[indexPath.row]
        cell!.lblTopic.text  = names[indexPath.row]
        self.addGesture(cell!.ImgView)
        self.addGesture(cell!.lblTopic)
      
        return cell!
    }
    
    func addGesture(clickView : UIView){
        let tapGesture = UITapGestureRecognizer(target: self, action:#selector(MainPAgeViewController.tappedImageView(_:)))
        tapGesture.numberOfTapsRequired = 1
        clickView.userInteractionEnabled = true
        clickView.addGestureRecognizer(tapGesture)
    }
    
    func tappedImageView(sender: UITapGestureRecognizer) {
      
            let productDetails  =  DetailPageViewController(nibName: "DetailPageViewController", bundle: NSBundle.mainBundle())
            self.navigationController?.pushViewController(productDetails, animated: true)
        
        }
        
    @IBAction func LetestBtn(sender: AnyObject) {
        
        let viewObject = self.storyboard?.instantiateViewControllerWithIdentifier("MainPAgeViewController") as? MainPAgeViewController
        print(self.navigationController)
        self.navigationController?.pushViewController(viewObject!, animated: true)

    }

    @IBAction func thaughtBtn(sender: AnyObject) {
        
        let viewObject = self.storyboard?.instantiateViewControllerWithIdentifier("ViewController") as? ViewController
        print(self.navigationController)
        self.navigationController?.pushViewController(viewObject!, animated: true)
        
    }
   
    @IBAction func shareBtnAction(sender: AnyObject) {
        
    }


}
