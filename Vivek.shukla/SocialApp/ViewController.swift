//
//  ViewController.swift
//  SocialApp
//
//  Created by Vivek Shukla on 27/08/16.
//  Copyright © 2016 Vivek Shukla. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tblHiehtConstnts: NSLayoutConstraint!
    var names = ["Joan D. Vinge", "Sonia Sotomayor", "Yehuda Brady", "Avril Lavigne", "Siniya Sonamaylya","Joan D. Vinge", "Sonia Sotomayor", "Yehuda Brady", "Avril Lavigne", "Siniya Sonamaylya"]
    var link = ["http://www.brainyquote.com/quotes/keywords/life_experiences.html", "http://www.brainyquote.com/quotes/keywords/life_experiences.html", " http://www.brainyquote.com/quotes/keywords/life_experiences.html",
                "http://www.brainyquote.com/quotes/keywords/life_experiences.html", "//www.brainyquote.com/quotes/keywords/life_experiences.html","http://www.brainyquote.com/quotes/keywords/life_experiences.html"]
    
    var thought = ["We are all born with a unique genetic blueprint, which lays out the basic characteristics of our personality as well as our physical health and appearance... And yet, we all know that life experiences do change us.",
                   "I do believe that every person has an equal opportunity to be a good and wise judge regardless of their background or life experiences", "We are increasingly open to understanding how we are all connected and that if we sink the ship that we are all on, we all drown. However, we have simultaneously become so focused on our own life experiences that we think we are alone","Inspiration for my music just comes from, you know, my life experiences."," Everyone has their own different life experiences which make them who they are. No two people's life experiences are the same. And mine are just unique","A face is a road map of someone's life. Without any need to amplify that or draw attention to it, there's a great deal that's communicated about who this person is and what their life experiences have been", "We are all born with a unique genetic blueprint, which lays out the basic characteristics of our personality as well as our physical health and appearance... And yet, we all know that life experiences do change us.", "I do believe that every person has an equal opportunity to be a good and wise judge regardless of their background or life experiences"]
    var images = [UIImage(named: "images"),UIImage(named: "images1"),UIImage(named: "images2"),UIImage(named: "images3"),UIImage(named: "images4"),UIImage(named: "image"),UIImage(named: "images1"),UIImage(named: "images2"),UIImage(named: "images3"),UIImage(named: "images4")]

    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
//        let logo = UIImage(named: "ThumbUp.png")
//        let imageView = UIImageView(image:logo)
//        self.navigationItem.titleView = imageView
        
        let search = UIBarButtonItem(image: UIImage(named: "search"),style: UIBarButtonItemStyle.Plain ,target: self, action: "OnMenuClicked:")
       self.navigationItem.rightBarButtonItem = search
    
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCellWithIdentifier("cell",  forIndexPath: indexPath) as? ProfileThoughtViewCell
       
        cell!.ImgPhoto.image = images[indexPath.row]
        cell!.nameLbl.text  = names[indexPath.row]
       // cell!.btnLink.buttonType = link[indexPath.row]
        cell!.lblthought.text = thought[indexPath.row]
        
        return cell!
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        tblHiehtConstnts.constant = tableView.contentSize.height
    }
    


}

