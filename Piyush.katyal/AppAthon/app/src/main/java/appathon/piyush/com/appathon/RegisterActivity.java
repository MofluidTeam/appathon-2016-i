package appathon.piyush.com.appathon;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import DBManager.DatabaseHandler;
import Model.RegisterData;
import Utils.Utils;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    private static final long TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initialise();
        getviewcontrols();

    }

    private void getviewcontrols() {
        Button do_register = (Button) findViewById(R.id.btn_do_register);
        do_register.setOnClickListener(this);
        TextView back_login = (TextView) findViewById(R.id.txt_login_back);
        back_login.setOnClickListener(this);
    }

    private void initialise() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_do_register:
                callforRegister();
                break;
            case R.id.txt_login_back:
                final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
                pDialog.setCancelable(false);
                pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                pDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.hide();
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                        finish();
                    }
                }, TIME_OUT);

                break;
            default:

        }

    }

    private void callforRegister() {

        EditText fname = (EditText) findViewById(R.id.FirstName);
        EditText lname = (EditText) findViewById(R.id.LastName);
        EditText email = (EditText) findViewById(R.id.email);
        EditText contact = (EditText) findViewById(R.id.mobile);
        EditText password = (EditText) findViewById(R.id.password);
        EditText confirm_password = (EditText) findViewById(R.id.confirm_password);
        if (validatedetails(fname, lname, email, contact, password, confirm_password))
            performregister(fname, lname, email, contact, password, confirm_password);
    }

    private boolean validatedetails(EditText fname, EditText lname, EditText email, EditText contact, EditText password, EditText confirm_password) {

        if (!password.getText().toString().equals(confirm_password.getText().toString())) {
            Toast.makeText(this, "Please enter same password", Toast.LENGTH_SHORT).show();
            password.setText("");
            confirm_password.setText("");
            return false;
        }
        if (fname.getText().length() < 1 || lname.getText().length() < 1 || contact.getText().length() < 10) {
            Toast.makeText(this, "Incorrect Details. Please enter again !", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!Utils.emailValidator(email.getText().toString())) {
            Toast.makeText(this, "Invalid Email ID", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }



    private void performregister(EditText fname, EditText lname, EditText email, EditText contact, EditText password, EditText confirm_password) {

        DatabaseHandler db = new DatabaseHandler(RegisterActivity.this, null, null, 2);
        RegisterData reg = new RegisterData();

        reg.setFirst_name(fname.getText().toString());
        reg.setLast_name(lname.getText().toString());
        reg.setUsername(email.getText().toString());
        reg.setMobile_number(contact.getText().toString());
        reg.setPassword(password.getText().toString());
        db.addregister(reg);

        Toast.makeText(getApplicationContext(), "Registered", Toast.LENGTH_LONG).show();
        final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pDialog.hide();
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                finish();
            }
        }, TIME_OUT);


    }


}
