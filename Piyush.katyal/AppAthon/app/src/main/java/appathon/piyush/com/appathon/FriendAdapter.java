package appathon.piyush.com.appathon;

/**
 * Created by piyush-ios on 27/8/16.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import DBManager.DatabaseHandler;
import Model.Friend;
import Utils.SessionManager;

public class FriendAdapter extends BaseAdapter {
    Context context;
    ArrayList<Friend> friendList;
    private static LayoutInflater inflater=null;

    public FriendAdapter(Activity mainActivity, ArrayList<Friend> friendList) {
        // TODO Auto-generated constructor stub
        context=mainActivity;
        this.friendList= friendList;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return friendList.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView friendname;
        Button addfriend;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.list_add, null);
        holder.friendname=(TextView) rowView.findViewById(R.id.name_friend);
        holder.addfriend=(Button) rowView.findViewById(R.id.btn_add_friend);
        holder.friendname.setText(friendList.get(position).getName());
        holder.addfriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFriend(position);
            }
        });
        return rowView;
    }

    private void addFriend(int position) {
        DatabaseHandler db = new DatabaseHandler(context, null, null, 2);
        SessionManager session = new SessionManager(context.getApplicationContext());
        String user = session.getUserDetails().get(SessionManager.KEY_EMAIL);
        db.addFriend(user,friendList.get(position).getEmail());
        Toast.makeText(context, "Added friend successfully", Toast.LENGTH_SHORT).show();
    }


}
