package appathon.piyush.com.appathon;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import java.util.ArrayList;

import DBManager.DatabaseHandler;
import Model.FeedData;
import Utils.Constants;
import Utils.SessionManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final long TIME_OUT = 1000;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;

    SessionManager session;
    private String username;
    private String firstname;
    private ExpandableHeightListView listview;
    private CustomFeedAdapter customAdapter;
    private ArrayList<FeedData> feedlist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getIntentData();
        getViewControls();
        getAllFeeds();

    }

    private void getAllFeeds() {
        DatabaseHandler db = new DatabaseHandler(MainActivity.this, null, null, 2);
        feedlist = db.getCurrentUserFeeds(username);
        if(feedlist!=null) {
            customAdapter = new CustomFeedAdapter(this, feedlist);
            listview.setExpanded(true);
            listview.setAdapter(customAdapter);
        }

    }

    private void getViewControls() {
        Button post_feed = (Button)findViewById(R.id.btn_post_feed);
        post_feed.setOnClickListener(this);
        ImageView add_friend = (ImageView)findViewById(R.id.foot_addfriend);
        add_friend.setOnClickListener(this);
        listview = (ExpandableHeightListView)findViewById(R.id.list_feeds);
    }

    private void getIntentData() {
       username = getIntent().getStringExtra(Constants.USERNAME);
       firstname = getIntent().getStringExtra(Constants.FIRSTNAME);
        getSupportActionBar().setTitle("Welcome "+ firstname+ " !");

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_post_feed:
                final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
                pDialog.setCancelable(false);
                pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                pDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.hide();
                        postmyfeed();
                    }
                }, TIME_OUT);
                break;
            case R.id.foot_addfriend:
                    Intent intent = new Intent(MainActivity.this, AddFriendActivity.class);
                    intent.putExtra(Constants.USERNAME, username);
                    intent.putExtra(Constants.FIRSTNAME, firstname);
                    startActivity(intent);
                break;


        }
    }

    private void postmyfeed() {

        DatabaseHandler db = new DatabaseHandler(MainActivity.this, null, null, 2);
        session = new SessionManager(getApplicationContext());
        TextView feed_text = (TextView)findViewById(R.id.txtv_feed_text);
        FeedData feedData = new FeedData();
        feedData.setFeed_user(session.getUserDetails().get(SessionManager.KEY_EMAIL));
        feedData.setFeed_text(feed_text.getText().toString());
        feedData.setFeed_likes(0);
        Log.d("Piyush","Feed posted added is "+ feedData.toString());
        db.addfeed(feedData);
        Toast.makeText(getApplicationContext(),"Feed posted successfully",Toast.LENGTH_SHORT).show();
        feedlist = db.getCurrentUserFeeds(username);
//        customAdapter.notifyDataSetChanged();
        customAdapter = new CustomFeedAdapter(this, feedlist);
        listview.setAdapter(customAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                performLogout();
                return true;
        }
        return true;
    }

    private void performLogout() {
        session= new SessionManager(getApplicationContext());
        session.logoutUser();
        final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
        pDialog.setCancelable(false);
        pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
        pDialog.show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),"Logged out successfully", Toast.LENGTH_SHORT).show();
                pDialog.hide();
                Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }, TIME_OUT);


    }

    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(getBaseContext(), "Press once again to exit!",
                    Toast.LENGTH_SHORT).show();        }
        back_pressed = System.currentTimeMillis();
    }

}
