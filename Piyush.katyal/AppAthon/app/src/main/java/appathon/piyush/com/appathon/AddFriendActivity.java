package appathon.piyush.com.appathon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import DBManager.DatabaseHandler;
import Model.Friend;
import Utils.Constants;

public class AddFriendActivity extends AppCompatActivity {

    private String username;
    private String firstname;
    private FriendAdapter friendAdapter;
    ArrayList<Friend> friendList;
    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);
        getIntentData();
        getviewControls();
        setList();
    }

    private void setList() {
        DatabaseHandler db = new DatabaseHandler(AddFriendActivity.this, null, null, 2);
        friendList = db.getAlluserList(username);
        friendAdapter = new FriendAdapter(AddFriendActivity.this,friendList);
        lv.setAdapter(friendAdapter);


    }

    private void getviewControls() {
         lv = (ListView)findViewById(R.id.list_friend);
    }

    private void getIntentData() {
        username = getIntent().getStringExtra(Constants.USERNAME);
        firstname = getIntent().getStringExtra(Constants.FIRSTNAME);
        getSupportActionBar().setTitle(firstname+ " , add your friends ");


    }
}
