package appathon.piyush.com.appathon;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import Model.RegisterData;
import Utils.Constants;

import org.w3c.dom.Text;

import java.util.HashMap;

import DBManager.DatabaseHandler;
import Utils.SessionManager;
import Utils.Utils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    SessionManager sessionManager;

    final Context context = this;
    private static int TIME_OUT = 1000;
    private static final int TIME_DELAY = 2000;
    private static long back_pressed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkforSession();
        setContentView(R.layout.activity_login);
        initialize();
        geViewControls();

    }

    private void checkforSession() {
        sessionManager = new SessionManager(getApplicationContext());
        if(sessionManager.isLoggedIn())
        {

            final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
            pDialog.setCancelable(false);
            pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            pDialog.show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    pDialog.hide();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    HashMap<String,String> user = sessionManager.getUserDetails();
                    intent.putExtra(Constants.USERNAME,user.get(SessionManager.KEY_EMAIL));
                    intent.putExtra(Constants.FIRSTNAME,user.get(SessionManager.KEY_NAME));
                    startActivity(intent);
                    overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    finish();
                }
            }, TIME_OUT);


        }

    }

    private void initialize() {

    }

    private void geViewControls() {
        TextView action_register = (TextView)findViewById(R.id.action_register);
        action_register.setOnClickListener(this);
        Button signin = (Button)findViewById(R.id.email_sign_in_button);
        signin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.action_register:
                final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
                pDialog.setCancelable(false);
                pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                pDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.hide();
                        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    }
                }, TIME_OUT);

                break;
            case R.id.email_sign_in_button:
                startLoginProcess();
                break;

            default :

        }
    }

    private void startLoginProcess() {

        EditText email = (EditText) findViewById(R.id.email);
        EditText password = (EditText)findViewById(R.id.password);
        if(validateDetails(email.getText().toString(), password.getText().toString()));
        performLogin(email, password);
    }

    private void performLogin(final EditText email, EditText password) {

        DatabaseHandler db = new DatabaseHandler(LoginActivity.this, null, null, 2);

        final RegisterData rd = db.getregister(email.getText().toString());
        if(rd!=null) {
            String StoredPassword = rd.getPassword();
            if (password.getText().toString().equals(StoredPassword)) {
                Toast.makeText(getApplicationContext(), "Login Successfully", Toast.LENGTH_LONG).show();
                sessionManager = new SessionManager(getApplicationContext());
                sessionManager.createLoginSession(rd.getFirst_name(),rd.getUsername());
                Log.d("Piyush", "Session Created");
                final ProgressDialog pDialog = new ProgressDialog(this, R.style.MyTheme);
                pDialog.setCancelable(false);
                pDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
                pDialog.show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.hide();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra(Constants.USERNAME, rd.getUsername());
                        intent.putExtra(Constants.FIRSTNAME,rd.getFirst_name());
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.fadein, R.anim.fadeout);

                    }
                }, TIME_OUT);


            } else {
                Toast.makeText(getApplicationContext(), "Username/Password incorrect", Toast.LENGTH_LONG).show();
                email.setText("");
                password.setText("");
            }
        }
        else
        {
            Toast.makeText(getApplicationContext(), "Username/Password incorrect", Toast.LENGTH_LONG).show();
            email.setText("");
            password.setText("");
        }


    }

    private boolean validateDetails(String email, String password) {
        if(email.length()<1 || password.length()<1) {
            Toast.makeText(context, "Mandatory Fields cannot be left empty", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(!Utils.emailValidator(email))
        {
            Toast.makeText(context, "Inavlid Email ID", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
            System.exit(1);
        } else {
            Toast.makeText(getBaseContext(), "Press once again to exit!",
                    Toast.LENGTH_SHORT).show();        }
        back_pressed = System.currentTimeMillis();
    }
}
