package DBManager;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import Model.FeedData;
import Model.Friend;
import Model.RegisterData;

public class DatabaseHandler extends SQLiteOpenHelper {

    public static final String KEY_ID = "id";
    public static final String KEY_FIRST_NAME = "first_name";
    public static final String KEY_lAST_NAME = "last_name";
    public static final String KEY_EMAIL_ID = "email_id";
    public static final String KEY_MOB_NO = "mobile_number";
    public static final String KEY_PASSWORD = "password";


    public static final String KEY_FEED_USERNAME = "username";
    public static final String KEY_FEED_ID = "feed_id";
    public static final String KEY_FEED_TEXT = "feed_text";
    public static final String KEY_FEED_LIKES = "feed_likes";
    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "GreenBook.db";
    // TABLE FOR USER
    private static final String TABLE_REGISTER = "register";
    // TABLE FOR FEEDS
    private static final String TABLE_FEEDS = "feeds";

    private static final String TABLE_FRIENDS = "friends";

    public static final String KEY_FRIEND_ID = "friend_id";
    public static final String FRIEND_HOST = "host";
    public static final String FRIEND_EMAILID = "friend_email";


    String password;

    public DatabaseHandler(Context context, Object name,
                           Object factory, int version) {
        // TODO Auto-generated constructor stub
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static String getKeyId() {
        return KEY_ID;
    }

    public static String getTableContacts() {
        return TABLE_REGISTER;
    }

    public static int getDatabaseVersion() {
        return DATABASE_VERSION;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        createAllTables(db);

    }

    private void createAllTables(SQLiteDatabase db) {
        final String CREATE_TABLE_USER = "CREATE TABLE " + TABLE_REGISTER + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_FIRST_NAME + " TEXT," + KEY_lAST_NAME + " TEXT," + KEY_EMAIL_ID + " TEXT,"
                + KEY_MOB_NO + " TEXT," + KEY_PASSWORD + " TEXT " + ")";

        db.execSQL(CREATE_TABLE_USER);

        final String CREATE_TABLE_FEEDS = "CREATE TABLE " + TABLE_FEEDS + "("
                + KEY_FEED_ID + " INTEGER PRIMARY KEY," + KEY_FEED_USERNAME + " TEXT," + KEY_FEED_LIKES + " INTEGER," + KEY_FEED_TEXT + " TEXT" + ")";

        db.execSQL(CREATE_TABLE_FEEDS);

        final String CREATE_TABLE_FRIENDS = "CREATE TABLE " + TABLE_FRIENDS + "("
                + KEY_FRIEND_ID + " INTEGER PRIMARY KEY," + FRIEND_HOST + " TEXT,"+ FRIEND_EMAILID + " TEXT" + ")";
        Log.d("Piyush ", "FRIENDS SQL " +CREATE_TABLE_FRIENDS);
        db.execSQL(CREATE_TABLE_FRIENDS);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REGISTER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FEEDS);

        // Create tables again
        onCreate(db);
    }

    //code to get the register
    public RegisterData getregister(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        //String selectquery="SELECT * FROM TABLE_REGISTER";
        Cursor cursor = db.query(TABLE_REGISTER, null, "email_id=?", new String[]{username}, null, null, null, null);

        if (cursor.getCount() < 1) {
            cursor.close();
            return null;
        } else if (cursor.getCount() >= 1 && cursor.moveToFirst()) {

            password = cursor.getString(cursor.getColumnIndex(KEY_PASSWORD));
            String fname = cursor.getString(cursor.getColumnIndex(KEY_FIRST_NAME));
            String lname = cursor.getString(cursor.getColumnIndex(KEY_lAST_NAME));
            RegisterData rd = new RegisterData();
            rd.setUsername(username);
            rd.setPassword(password);
            rd.setFirst_name(fname);
            rd.setLast_name(lname);

            cursor.close();
            return rd;

        }

        return null;


    }
    

    public ArrayList<Friend> getAlluserList(String username)
    {
        ArrayList<Friend> allusers = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String param[] = new String[1];
        param[0]= username;
        //String selectquery="SELECT * FROM TABLE_REGISTER";
        Cursor cursor = db.query(TABLE_REGISTER, null, "email_id!=?", new String[]{username}, null, null, null, null);

        if (cursor.getCount() < 1) {
            cursor.close();
            return allusers;
        } else if (cursor.getCount() >=1 && cursor.moveToFirst()) {
            try {
                do {
                    String fname = cursor.getString(cursor.getColumnIndex(KEY_FIRST_NAME));
                    String lname = cursor.getString(cursor.getColumnIndex(KEY_lAST_NAME));
                    String email = cursor.getString(cursor.getColumnIndex(KEY_EMAIL_ID));
                    allusers.add(new Friend((fname + " "+ lname), email));
                }while (cursor.moveToNext());
            }catch (Exception e)
            {
                e.printStackTrace();
            }
            finally {
                cursor.close();
                db.close();
            }

            return allusers;
        } else

        return allusers;
    }


    public void addregister(RegisterData registerdata)
    // code to add the new register
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_NAME, registerdata.getFirst_name()); // register first Name
        values.put(KEY_lAST_NAME, registerdata.getLast_name()); // register last name
        values.put(KEY_EMAIL_ID, registerdata.getUsername());//register email id
        values.put(KEY_MOB_NO, registerdata.getMobile_number());//register mobile no
        values.put(KEY_PASSWORD, registerdata.getPassword());
        // Inserting Row

        db.insert(TABLE_REGISTER, null, values);
        db.close(); // Closing database connection

    }


    public void addfeed(FeedData feeddata)
    // code to add the new feed
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();         //add text of feed
        values.put(KEY_FEED_USERNAME, feeddata.getFeed_user());
        values.put(KEY_FEED_LIKES, feeddata.getFeed_likes());//register email id
        values.put(KEY_FEED_TEXT, feeddata.getFeed_text());
        // Inserting Row

        db.insert(TABLE_FEEDS, null, values);
        db.close(); // Closing database connection

    }

    public void addFriend(String user, String friendemail)
    // code to add the new feed
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();         //add text of feed
        values.put(FRIEND_HOST, user);
        values.put(FRIEND_EMAILID, friendemail);//register email id
        // Inserting Row

        db.insert(TABLE_FRIENDS, null, values);
        db.close(); // Closing database connection

    }

    public ArrayList<FeedData> getCurrentUserFeeds(String username) {
        SQLiteDatabase db = this.getReadableDatabase();
        String param[] = new String[1];
        param[0]= username;
        //String selectquery="SELECT * FROM TABLE_REGISTER";
        Cursor cursor = db.query(TABLE_FEEDS, null, "username=?", new String[]{username}, null, null, null, null);

        if (cursor.getCount() < 1) {
            cursor.close();
            return null;
        } else if (cursor.getCount() >=1 && cursor.moveToFirst()) {
        ArrayList<FeedData> feedlist = new ArrayList<>();
            try {
                do {
                    FeedData feed = new FeedData();
                    feed.setFeed_id(cursor.getInt(cursor.getColumnIndex(KEY_FEED_ID)));
                    feed.setFeed_user(cursor.getString(cursor.getColumnIndex(KEY_FEED_USERNAME)));
                    feed.setFeed_text(cursor.getString(cursor.getColumnIndex(KEY_FEED_TEXT)));
                    feed.setFeed_likes(cursor.getInt(cursor.getColumnIndex(KEY_FEED_LIKES)));
                    feedlist.add(feed);
                }while (cursor.moveToNext());
                }catch (Exception e)
            {
                e.printStackTrace();
            }
                finally {
                cursor.close();
                db.close();
            }

            return feedlist;
        } else
            return null;
    }


    public String getDatabaseName() {
        return DATABASE_NAME;
    }


}
