package Model;

/**
 * Created by piyush-ios on 27/8/16.
 */
public class FeedData {



    private int feed_id;
    private String feed_text;
    private int feed_likes;
    private String feed_user;

    public FeedData() {
    }

    public FeedData(int feed_id, String feed_text, int feed_likes, String feed_user) {
        this.feed_id = feed_id;
        this.feed_text = feed_text;
        this.feed_likes = feed_likes;
        this.feed_user = feed_user;
    }

    public int getFeed_id() {
        return feed_id;
    }

    public void setFeed_id(int feed_id) {
        this.feed_id = feed_id;
    }

    public String getFeed_text() {
        return feed_text;
    }

    public void setFeed_text(String feed_text) {
        this.feed_text = feed_text;
    }

    public int getFeed_likes() {
        return feed_likes;
    }

    public void setFeed_likes(int feed_likes) {
        this.feed_likes = feed_likes;
    }

    public String getFeed_user() {
        return feed_user;
    }

    public void setFeed_user(String feed_user) {
        this.feed_user = feed_user;
    }
}
