package com.example.ebizon201.employeebook.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ebizon201.employeebook.R;

/**
 * Created by ebizon201 on 27/8/16.
 */
public class ViewUserProfile  extends Fragment{

    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragmen_sign_in, null);
        initialized();
        getViewControlls();
        return  rootView;
    }

    private void getViewControlls() {

    }

    private void initialized() {

    }
}
