package com.example.ebizon201.employeebook;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.example.ebizon201.employeebook.fragment.SignInUpFragment;
import com.example.ebizon201.employeebook.fragment.WelcomeFragment;

/**
 * Created by ebizon201 on 27/8/16.
 */
public class WelcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);
        callFragment();

    }
    private void callFragment() {
        WelcomeFragment fragment2 = new WelcomeFragment();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(this, fragmentTransaction, fragment2, fragment2, R.id.employee_detail_container);

        fragmentTransactionExtended.addTransition(7);
        fragmentTransactionExtended.commit();
    }
}