package com.example.ebizon201.employeebook.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.ebizon201.employeebook.model.SignUpUser;

import java.util.ArrayList;

/**
 * Created by ebizon on 24/11/15.
 */
public class MyDataBaseAdapter extends SQLiteOpenHelper {
    // Database Name
    private static final String DATABASE_NAME = "cart_list_db";

    // Current version of database
    private static final int DATABASE_VERSION = 1;

   /* User profile table*/
    //table name
    private static final String TABLE_USER_PROFILE= "user_profile_table";
    // column name

    /*primary key*/
//String fname,lname,dob,department,degination,salry,userId,mobile,password,emailid;

    private static final String KEY_FIRST_NAME = "firstname";
    private static final String KEY_LAST_NAME = "lastname";
    private static final String KEY_DOB = "dob";//this id provide by server
    private static final String KEY_DEPARTMENT = "department";//this id provide by server
    private static final String KEY_DEGINATION = "degination";//this id provide by server
    private static final String KEY_SALARY = "salry";//this id provide by server
    private static final String KEY_UMOBILE = "mobile";//this is email id
    private static final String KEY_PASSWORD = "password";
    private static final String KEY_EMAIL_ID = "email_id";

    //create table
    private static final String CREATE_TABLE_USER="CREATE TABLE "
            + TABLE_USER_PROFILE + "("
            + KEY_EMAIL_ID + " TEXT  PRIMARY KEY,"
            + KEY_FIRST_NAME + " TEXT,"
            + KEY_LAST_NAME + " TEXT,"
            + KEY_DOB + " TEXT,"
            + KEY_DEPARTMENT + " TEXT,"
            + KEY_DEGINATION + " TEXT,"
            + KEY_SALARY + " TEXT,"
            + KEY_UMOBILE + " TEXT,"
            + KEY_PASSWORD + " TEXT);";

    private static final String TAG = "MyDataBaseAdapter";

    public MyDataBaseAdapter(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        TAG.getClass().getSimpleName();
    }

    public MyDataBaseAdapter(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_USER);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CREATE_TABLE_USER);

        onCreate(sqLiteDatabase);

    }


    /*insert data into user profile table*/
    public long inserUserProfiletData(SignUpUser item) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating content values
        Log.d(TAG, "inserUserProfiletData() called with: " + "primary key  = [" +  item.getFname() + "]");
        ContentValues values = new ContentValues();

        values.put(KEY_FIRST_NAME, item.getFname());
        values.put(KEY_LAST_NAME, item.getLname());
        values.put(KEY_PASSWORD, item.getPassword());
        values.put(KEY_EMAIL_ID, item.getEmailid());
        values.put(KEY_DOB, item.getDob());
        values.put(KEY_DEPARTMENT, item.getDepartment());
        values.put(KEY_DEGINATION, item.getDegination());
        values.put(KEY_SALARY, item.getSalry());
        values.put(KEY_UMOBILE, item.getMobile());


        return db.insert(TABLE_USER_PROFILE, null, values);
    }
    public int updateUserProfileData(SignUpUser item) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Creating content values
        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_NAME, item.getFname());
        values.put(KEY_LAST_NAME, item.getLname());
        values.put(KEY_PASSWORD, item.getPassword());
        //values.put(KEY_USER_NAME, item.getUsername());
        // update row in students table base on students.is value
        return db.update(TABLE_USER_PROFILE, values, KEY_EMAIL_ID + " = ?",
                new String[] { String.valueOf(item.getUserId()) });
    }

    public ArrayList<SignUpUser> getUserProfile() {
        ArrayList<SignUpUser> userProfile = new ArrayList<SignUpUser>();

        String selectQuery = "SELECT  * FROM " + TABLE_USER_PROFILE;
        Log.d(TAG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        Log.d(TAG, "getUserProfile() called with:number of record  " +c.getCount()+ "");


        // looping through all rows and adding to list
        if (c.moveToFirst()) {

            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(0)+ "");
            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(1)+ "");
            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(2)+ "");
            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(3)+ "");
            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(4)+ "");
            Log.d(TAG, "getUserProfile() called with:number of record  " +c.getString(5)+ "");
            do {

                //SignUpUser(String fname, String salry, String degination, String dob, String lname, String department, String userId, String mobile, String password, String emailid)
                SignUpUser item = new SignUpUser(c.getString(c.getColumnIndex(KEY_FIRST_NAME)),c.getString(c.getColumnIndex(KEY_SALARY)),c.getString(c.getColumnIndex(KEY_DEGINATION)),c.getString(c.getColumnIndex(KEY_DOB)),c.getString(c.getColumnIndex(KEY_LAST_NAME)),c.getString(c.getColumnIndex(KEY_DEPARTMENT)),c.getString(c.getColumnIndex(KEY_EMAIL_ID)),c.getString(c.getColumnIndex(KEY_UMOBILE)),c.getString(c.getColumnIndex(KEY_PASSWORD)),c.getString(c.getColumnIndex(KEY_EMAIL_ID)));
                userProfile.add(item);
            } while (c.moveToNext());
        }

        db.close();
        return userProfile;
    }
    public boolean isUserAvailable(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        // SELECT * FROM students WHERE id = ?;
        String selectQuery = "SELECT  * FROM " + TABLE_USER_PROFILE + " WHERE " + KEY_EMAIL_ID + " = ? ";
        Log.d(TAG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, new String[]{id});



        if ( c.getCount()>0) {
            db.close();
            return true;
        }
        db.close();
        return false;
    }

}
