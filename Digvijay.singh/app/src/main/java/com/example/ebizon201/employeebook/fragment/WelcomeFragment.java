package com.example.ebizon201.employeebook.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.example.ebizon201.employeebook.R;
import com.example.ebizon201.employeebook.adapter.MyWelcomeAdapter;
import com.example.ebizon201.employeebook.data.MyDataBaseAdapter;
import com.example.ebizon201.employeebook.model.SignUpUser;

import java.util.ArrayList;

/**
 * Created by ebizon201 on 27/8/16.
 */
public class WelcomeFragment extends Fragment  {
    private View rootView;
    private EditText edtUserId,edtPassword;
    private Button btnLogin;
    private String TAG;
    private Activity activity;
    private ListView listViewId;
    private ArrayList<SignUpUser> userList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragmen_welcome, null);
        initialized();
        getViewControlls();
        setAdapter();
        return  rootView;
    }

    private void getViewControlls() {
        listViewId=(ListView) rootView.findViewById(R.id.listViewId);

        listViewId.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                SignUpUser employee = userList.get(position);
                EmployeeDetailFragment fragment2 = new EmployeeDetailFragment(employee);
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(activity, fragmentTransaction, fragment2, fragment2, R.id.rl_welcome_container);

                fragmentTransactionExtended.addTransition(7);
                fragmentTransactionExtended.commit();

            }
        });

    }

    private void initialized() {
        activity=getActivity();
        MyDataBaseAdapter dbAdapter = new MyDataBaseAdapter(getActivity());
         userList = dbAdapter.getUserProfile();


    }

    private void setAdapter() {
        MyWelcomeAdapter myAdapter=new MyWelcomeAdapter(activity,userList);
        listViewId.setAdapter(myAdapter);

    }
}