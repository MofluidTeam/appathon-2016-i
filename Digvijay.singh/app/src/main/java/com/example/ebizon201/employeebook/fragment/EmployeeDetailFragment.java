package com.example.ebizon201.employeebook.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ebizon201.employeebook.R;
import com.example.ebizon201.employeebook.model.SignUpUser;

import java.util.ArrayList;

/**
 * Created by ebizon201 on 27/8/16.
 */
@SuppressLint("ValidFragment")
public class EmployeeDetailFragment  extends Fragment {
    private final SignUpUser employee;
    private View rootView;
    private EditText edtUserId, edtPassword;
    private Button btnLogin;
    private String TAG;
    private Activity activity;
    private ListView listViewId;
    private ArrayList<SignUpUser> userList;

    public EmployeeDetailFragment(SignUpUser employee) {
        this.employee=employee;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragmen_employee_detail, null);
        initialized();
        getViewControlls();
        return rootView;
    }

    private void getViewControlls() {
        TextView txtVFName = (TextView) rootView.findViewById(R.id.txtVFName);
        TextView txtVLname = (TextView) rootView.findViewById(R.id.txtVLname);
        TextView txtVMobile = (TextView) rootView.findViewById(R.id.txtVMobile);
        TextView txtVEmailId = (TextView) rootView.findViewById(R.id.txtVEmailId);
        TextView txtVCompany = (TextView) rootView.findViewById(R.id.txtVCompany);
        TextView txtVDesignation = (TextView) rootView.findViewById(R.id.txtVDesignation);
        TextView txtVSkills = (TextView) rootView.findViewById(R.id.txtVSkills);
        TextView txtVSalary = (TextView) rootView.findViewById(R.id.txtVSalary);
        ImageView imgCall = (ImageView) rootView.findViewById(R.id.imgCall);
        ImageView imgMessage = (ImageView) rootView.findViewById(R.id.imgMessage);

        txtVFName.setText(employee.getFname());
        txtVLname.setText(employee.getLname());
        txtVMobile.setText(employee.getMobile());
        txtVEmailId.setText(employee.getEmailid());
        txtVSalary.setText(employee.getSalry());
        txtVCompany.setText("Ebizon");
        txtVDesignation.setText(employee.getDegination());
        txtVSkills.setText("C,C++,JAVA,Android,IOS,PHP,Magento,Drupal,Windows,Swift");
        imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"+"995322693"));
                startActivity(callIntent);

            }
        });
        imgMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( "sms:" + "9953226936"));
                intent.putExtra( "sms_body", " I want to contact you" );
                startActivity(intent);

            }
        });

    }

    private void initialized() {

    }
}