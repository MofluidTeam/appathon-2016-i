package com.example.ebizon201.employeebook.fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.example.ebizon201.employeebook.R;

/**
 * Created by ebizon201 on 27/8/16.
 */
public class SignInUpFragment extends Fragment {
    private View rootView;
    private Button txtVLogin;
    private Button txtVRegistration;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragmen_siginup, null);
        initialized();
        getControlls();
        return  rootView;
    }

    private void getControlls() {
         txtVLogin = (Button) rootView.findViewById(R.id.txtVLogin);
        txtVLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignInFragment fragment2 = new SignInFragment();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), fragmentTransaction, new SignInUpFragment(), fragment2, R.id.siginUpContainer);

                fragmentTransactionExtended.addTransition(7);
                fragmentTransactionExtended.commit();

            }
        });
        txtVRegistration=(Button) rootView.findViewById(R.id.txtVRegistration);
        txtVRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistrationFragment fragment2 = new RegistrationFragment();
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(getActivity(), fragmentTransaction, new SignInUpFragment(), fragment2, R.id.siginUpContainer);

                fragmentTransactionExtended.addTransition(7);
               fragmentTransactionExtended.commit();

            }
        });


    }

    private void initialized() {

    }



}
