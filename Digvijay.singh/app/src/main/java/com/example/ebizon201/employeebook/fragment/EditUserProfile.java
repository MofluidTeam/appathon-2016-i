package com.example.ebizon201.employeebook.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ebizon201.employeebook.R;
import com.example.ebizon201.employeebook.Util;
import com.example.ebizon201.employeebook.constant.MyConstantDetaMember;
import com.example.ebizon201.employeebook.data.MyDataBaseAdapter;
import com.example.ebizon201.employeebook.model.SignUpUser;

/**
 * Created by ebizon201 on 27/8/16.
 */
@SuppressLint("ValidFragment")
public class EditUserProfile extends Fragment implements View.OnClickListener {
    private final SignUpUser user;
    private View rootView;
    private EditText edtFname,edtLname,edtEmail,edtMobile,edtUserid,edtDegination,edtPassword;
    private Activity activity;

    @SuppressLint("ValidFragment")
    public EditUserProfile(SignUpUser user) {
        this.user=user;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_edit_profile, null);
        initialized();
        getControlls();
        Button register_ = (Button) rootView.findViewById(R.id.register_);
        register_.setOnClickListener(this);

        return  rootView;
    }
    private void getControlls() {
        edtFname = (EditText) rootView.findViewById(R.id.edtFname);
        edtLname = (EditText) rootView.findViewById(R.id.edtLname);
        edtEmail = (EditText) rootView.findViewById(R.id.edtEmail);
        TextView txtVEmailId = (TextView) rootView.findViewById(R.id.txtVEmailId);
        edtEmail.setVisibility(View.GONE);
        txtVEmailId.setVisibility(View.GONE);
        edtMobile = (EditText) rootView.findViewById(R.id.edtMobile);
        edtUserid = (EditText) rootView.findViewById(R.id.edtUserid);
        edtDegination = (EditText) rootView.findViewById(R.id.edtDegination);
        edtPassword = (EditText) rootView.findViewById(R.id.edtPassword);

        edtFname.setText(user.getFname());
        edtLname.setText(user.getLname());
        edtEmail.setText(user.getEmailid());
        edtMobile.setText(user.getMobile());
        edtUserid.setText(user.getUserId());
        edtDegination.setText(user.getDegination());
        edtPassword.setText(user.getPassword());
    }

    private void initialized() {
        activity=getActivity();

    }

    @Override
    public void onClick(View v) {

        if(isAllFielFilled()) {
            storeData();
            resetData();
            SharedPreferences sharedpreferences = activity.getSharedPreferences(MyConstantDetaMember.USER_SESSION, Context.MODE_PRIVATE);
            SharedPreferences.Editor preference = sharedpreferences.edit();
            preference.putBoolean(MyConstantDetaMember.isLogin,true);
            preference.putString(MyConstantDetaMember.userEmailId,edtEmail.getText().toString());
            preference.putString(MyConstantDetaMember.userName,edtLname.getText().toString());
            preference.commit();
            Toast.makeText(activity, activity.getResources().getString(R.string.profile_successfully_updated), Toast.LENGTH_SHORT).show();
        }

    }

    private void resetData() {
        edtFname.setText("");
        edtLname.setText("");
        edtEmail.setText("");
        edtMobile.setText("");
        edtUserid.setText("");
        edtDegination.setText("");
        edtPassword.setText("");

    }

    private void storeData() {

        String fnam=edtFname.getText().toString();
        String lnam=edtLname.getText().toString();
        String emailid=edtEmail.getText().toString();
        String mob=edtMobile.getText().toString();
        String userid=edtUserid.getText().toString();
        String degination=edtDegination.getText().toString();
        String password=edtPassword.getText().toString();
        //SignUpUser(String fname, String salry, String degination, String dob, String lname, String department, String userId, String mobile, String password, String emailid)
        String salry="10000";
        String dob="15/Aug/2000";
        String department="IT";
        SignUpUser user = new SignUpUser(fnam, salry, degination, dob, lnam, department, userid, mob, password, emailid);
        MyDataBaseAdapter dbAdapter = new MyDataBaseAdapter(getActivity());
        long insrtid= dbAdapter.inserUserProfiletData(user);

    }

    private boolean isAllFielFilled() {
        String message="Please enter ";
        String fnam=edtFname.getText().toString();
        String lnam=edtLname.getText().toString();
        String emailid=edtEmail.getText().toString();
        String mob=edtMobile.getText().toString();
        String userid=edtUserid.getText().toString();
        String degination=edtDegination.getText().toString();
        String password=edtPassword.getText().toString();

        if(!Util.isNotEmpty(fnam)) {
            Toast.makeText(activity, message+"first name", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!Util.isNotEmpty(lnam))
        {
            Toast.makeText(activity, message+"last name", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!Util.isNotEmpty(emailid))
        {
            Toast.makeText(activity, message+"email id", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!Util.isNotEmpty(mob))
        {
            Toast.makeText(activity, message+"mobile number ", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!Util.isNotEmpty(userid))
        {
            Toast.makeText(activity, message+"user id", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!Util.isNotEmpty(degination))
        {
            Toast.makeText(activity, message+"fidestination", Toast.LENGTH_SHORT).show();
            return false;
        }
        else if(!Util.isNotEmpty(password))
        {
            Toast.makeText(activity, message+"password", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;


    }
}
