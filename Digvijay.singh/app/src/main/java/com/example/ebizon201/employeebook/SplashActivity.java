package com.example.ebizon201.employeebook;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.ebizon201.employeebook.constant.MyConstantDetaMember;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        long delayMillis = 3000;
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                SharedPreferences sharedpreferences =getSharedPreferences(MyConstantDetaMember.USER_SESSION, Context.MODE_PRIVATE);

                boolean flagIsLogin = sharedpreferences.getBoolean(MyConstantDetaMember.isLogin, false);
                Intent mainIntent;
                if(!flagIsLogin) {
                     mainIntent = new Intent(SplashActivity.this, SignupInActivity.class);
                }
                else
                    mainIntent = new Intent(SplashActivity.this, Main2Activity.class);

                SplashActivity.this.startActivity(mainIntent);
                SplashActivity.this.finish();
            }
        }, delayMillis);
    }
}
