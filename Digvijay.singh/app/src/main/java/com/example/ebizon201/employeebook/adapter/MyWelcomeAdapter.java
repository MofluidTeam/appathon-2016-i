package com.example.ebizon201.employeebook.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ebizon201.employeebook.R;
import com.example.ebizon201.employeebook.model.SignUpUser;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by ebizon201 on 27/8/16.
 */
public class MyWelcomeAdapter extends BaseAdapter {
    private final Activity activity;
    private final ArrayList<SignUpUser> userList;
    LayoutInflater inflater;

    public MyWelcomeAdapter(Activity activity, ArrayList<SignUpUser> userList) {
        this.activity=activity;
        this.userList=userList;
        inflater=(LayoutInflater)activity.getLayoutInflater();


    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView=inflater.inflate(R.layout.row_employee_list,null);
        TextView txtVNameId = (TextView) convertView.findViewById(R.id.txtVNameId);
        TextView txtVCountry = (TextView) convertView.findViewById(R.id.txtVCountry);
        TextView txtVCmpmnyName = (TextView) convertView.findViewById(R.id.txtVCmpmnyName);
        TextView txtVDepartMent = (TextView) convertView.findViewById(R.id.txtVDepartMent);
        ImageView imgUser = (ImageView) convertView.findViewById(R.id.imgUser);

        SignUpUser user = userList.get(position);
        txtVNameId.setText(user.getFname()+" "+user.getLname());
        return convertView;
    }
}
