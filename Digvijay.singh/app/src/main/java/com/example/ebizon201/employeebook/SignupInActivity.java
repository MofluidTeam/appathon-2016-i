package com.example.ebizon201.employeebook;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.example.ebizon201.employeebook.fragment.SignInUpFragment;

/**
 * Created by ebizon201 on 27/8/16.
 */
public class SignupInActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signiinup);
        callFragment();
    }

    private void callFragment() {
        SignInUpFragment fragment2 = new SignInUpFragment();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(this, fragmentTransaction, fragment2, fragment2, R.id.siginUpContainer);

        fragmentTransactionExtended.addTransition(7);
      fragmentTransactionExtended.commit();
    }

    @Override
    public void onBackPressed() {
        popCurrentFragment();

    }
    private void popCurrentFragment() {
        FragmentManager fm = getFragmentManager();
        int count = fm.getBackStackEntryCount();
        if (count > 1) {
           boolean flag= fm.popBackStackImmediate();

        }
        else
            this.finish();


    }
}
