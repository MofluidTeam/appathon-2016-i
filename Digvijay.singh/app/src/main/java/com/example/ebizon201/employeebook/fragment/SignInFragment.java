package com.example.ebizon201.employeebook.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.example.ebizon201.employeebook.Main2Activity;
import com.example.ebizon201.employeebook.R;
import com.example.ebizon201.employeebook.WelcomeActivity;
import com.example.ebizon201.employeebook.constant.MyConstantDetaMember;
import com.example.ebizon201.employeebook.data.MyDataBaseAdapter;
import com.example.ebizon201.employeebook.model.SignUpUser;

import java.util.ArrayList;

/**
 * Created by ebizon201 on 27/8/16.
 */
public class SignInFragment  extends Fragment implements View.OnClickListener {
    private View rootView;
    private EditText edtUserId,edtPassword;
    private Button btnLogin;
    private String TAG;
    private Activity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         rootView = inflater.inflate(R.layout.fragmen_sign_in, null);
        initialized();
        getViewControlls();
        btnLogin.setOnClickListener(this);
        return  rootView;
    }

    private void initialized() {
        TAG=getClass().getSimpleName();
        activity=getActivity();
    }

    private void getViewControlls() {

        edtUserId = (EditText) rootView.findViewById(R.id.edtUserId);
        edtPassword = (EditText) rootView.findViewById(R.id.edtPassword);
        btnLogin=(Button) rootView.findViewById(R.id.btnLogin);
        TextView txtVSignupNow = (TextView) rootView.findViewById(R.id.txtVSignupNow);
        String msg=activity.getResources().getString(R.string.not_a_member_signup_now);
        txtVSignupNow.setText(Html.fromHtml(msg));
        txtVSignupNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistrationFragment regi=new RegistrationFragment();
                callFragment(regi);
            }
        });
    }


    @Override
    public void onClick(View v) {
        if(isUserFound())
        {

            Intent intent = new Intent(activity, Main2Activity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
            Toast.makeText(activity, "Please enter valid entery", Toast.LENGTH_SHORT).show();

    }

    public boolean isUserFound() {
        MyDataBaseAdapter dbAdapter = new MyDataBaseAdapter(getActivity());
        ArrayList<SignUpUser> userList = dbAdapter.getUserProfile();
        String userId = edtUserId.getText().toString();
        String userpass = edtPassword.getText().toString();
        for(int i=0;i<userList.size();i++) {
            SignUpUser user = userList.get(i);
            Log.d(TAG, "isUserFound() called with: " + user.getEmailid()+"  "+user.getFname());
        }
        for(int i=0;i<userList.size();i++)
        {
            SignUpUser user = userList.get(i);
            if(user.getEmailid().equals(userId) && user.getPassword().equals(userpass)) {
                setUserSession(user);
                return true;
            }
        }

        return false;
    }

    private void setUserSession(SignUpUser user) {
        SharedPreferences sharedpreferences = activity.getSharedPreferences(MyConstantDetaMember.USER_SESSION, Context.MODE_PRIVATE);
        SharedPreferences.Editor preference = sharedpreferences.edit();
        preference.putBoolean(MyConstantDetaMember.isLogin,true);
        preference.putString(MyConstantDetaMember.userEmailId,user.getEmailid());
        preference.putString(MyConstantDetaMember.userName,user.getFname()+" "+user.getLname());
        preference.commit();
    }
    private void callFragment(Fragment fragment2) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(activity, fragmentTransaction, fragment2, fragment2, R.id.siginUpContainer);
        fragmentTransactionExtended.addTransition(7);
        fragmentTransactionExtended.commit();
    }
}
