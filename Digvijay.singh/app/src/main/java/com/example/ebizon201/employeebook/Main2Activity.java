package com.example.ebizon201.employeebook;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.desarrollodroide.libraryfragmenttransactionextended.FragmentTransactionExtended;
import com.example.ebizon201.employeebook.constant.MyConstantDetaMember;
import com.example.ebizon201.employeebook.data.MyDataBaseAdapter;
import com.example.ebizon201.employeebook.fragment.EditUserProfile;
import com.example.ebizon201.employeebook.fragment.EmployeeDetailFragment;
import com.example.ebizon201.employeebook.fragment.WelcomeFragment;
import com.example.ebizon201.employeebook.model.SignUpUser;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private MyDataBaseAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        initialized();

        WelcomeFragment fragment2 = new WelcomeFragment();
        callFragment(fragment2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initialized() {

         dbAdapter = new MyDataBaseAdapter(this);
    }

    private void setUserDetails() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        TextView txtVName = (TextView) navigationView.findViewById(R.id.txtVName);
        TextView txtVProfile = (TextView) navigationView.findViewById(R.id.txtVProfile);

        SharedPreferences sharedpreferences = getSharedPreferences(MyConstantDetaMember.USER_SESSION, Context.MODE_PRIVATE);
        /*txtVName.setText("hghghghhhghg");
        txtVProfile.setText("lkkk;");*/
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        }
        popCurrentFragment();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_view_profile) {
           viewUserProfoile();
        } else if (id == R.id.nav_edit_profile) {
            editUserProfile();
        } else if (id == R.id.nav_manage) {

            WelcomeFragment fr=new WelcomeFragment();
            callFragment(fr);

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        else if (id == R.id.nav_logout) {
            SharedPreferences sharedpreferences = this.getSharedPreferences(MyConstantDetaMember.USER_SESSION, Context.MODE_PRIVATE);
            SharedPreferences.Editor edit = sharedpreferences.edit();
            edit.clear().commit();
            Intent mainIntent = new Intent(this, SignupInActivity.class);
             startActivity(mainIntent);
            this.finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void callWelcomeFragment() {

    }

    private void viewUserProfoile() {
        SharedPreferences sharedpreferences =getSharedPreferences(MyConstantDetaMember.USER_SESSION, Context.MODE_PRIVATE);

       String userId=sharedpreferences.getString(MyConstantDetaMember.userEmailId,null);

        ArrayList<SignUpUser> userList = dbAdapter.getUserProfile();
        for(int i=0;i<userList.size();i++) {

            SignUpUser user = userList.get(i);
            if(userId!=null && userId.equals(user.getEmailid()))
            {
                EmployeeDetailFragment fragment=new EmployeeDetailFragment(user);
                callFragment(fragment);
                break;
            }
        }

    }


    private void editUserProfile() {
        SharedPreferences sharedpreferences =getSharedPreferences(MyConstantDetaMember.USER_SESSION, Context.MODE_PRIVATE);
        String userId=sharedpreferences.getString(MyConstantDetaMember.userEmailId,null);

        ArrayList<SignUpUser> userList = dbAdapter.getUserProfile();
        for(int i=0;i<userList.size();i++) {

            SignUpUser user = userList.get(i);
            if(userId!=null && userId.equals(user.getEmailid()))
            {
                EditUserProfile fragment=new EditUserProfile(user);
                callFragment(fragment);
                break;
            }
        }

    }

    private void callFragment(Fragment fragment2) {

        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        FragmentTransactionExtended fragmentTransactionExtended = new FragmentTransactionExtended(this, fragmentTransaction, fragment2, fragment2, R.id.rl_welcome_container);

        fragmentTransactionExtended.addTransition(7);
        fragmentTransactionExtended.commit();
    }


    private void popCurrentFragment() {
        FragmentManager fm = getFragmentManager();
        int count = fm.getBackStackEntryCount();
        if (count > 1) {
            boolean flag= fm.popBackStackImmediate();

        }
        else
            this.finish();


    }
}
