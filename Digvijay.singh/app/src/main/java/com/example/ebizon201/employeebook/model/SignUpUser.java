package com.example.ebizon201.employeebook.model;

/**
 * Created by ebizon201 on 27/8/16.
 */
public class SignUpUser {
    String fname,lname,dob,department,degination,salry,userId,mobile,password,emailid;

    public SignUpUser(String fname, String salry, String degination, String dob, String lname, String department, String userId, String mobile, String password, String emailid) {
        this.fname = fname;
        this.salry = salry;
        this.degination = degination;
        this.dob = dob;
        this.lname = lname;
        this.department = department;
        this.userId = userId;
        this.mobile = mobile;
        this.password = password;
        this.emailid = emailid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getSalry() {
        return salry;
    }

    public void setSalry(String salry) {
        this.salry = salry;
    }

    public String getDegination() {
        return degination;
    }

    public void setDegination(String degination) {
        this.degination = degination;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }
}
