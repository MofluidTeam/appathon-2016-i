//
//  DBManager.h
//  EbizonInternalMarket
//
//  Created by Vivek_ebizon on 27/08/16.
//  Copyright © 2016 Sadique_ebizon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface DBManager : NSObject
{
    NSString *databasePath;
}
+(DBManager*)getSharedInstance;
-(BOOL)createDB;
-(BOOL) saveData:(NSString*)name email:(NSString*)email
      employeeID:(NSString*)employeeID mobileno:(NSString*)mobileno;
-(NSArray*) findByRegisterNumber:(NSString*)email;
-(NSMutableArray*) findAllList;
@end
