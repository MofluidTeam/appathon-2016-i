//
//  main.m
//  EbizonInternalMarket
//
//  Created by Vivek_ebizon on 27/08/16.
//  Copyright © 2016 Sadique_ebizon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
