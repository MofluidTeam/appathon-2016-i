//
//  EmployeeViewController.h
//  EbizonInternalMarket
//
//  Created by Vivek_ebizon on 27/08/16.
//  Copyright © 2016 Sadique_ebizon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
@interface EmployeeViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
