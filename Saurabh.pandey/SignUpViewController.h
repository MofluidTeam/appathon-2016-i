//
//  SignUpViewController.h
//  EbizonInternalMarket
//
//  Created by Vivek_ebizon on 27/08/16.
//  Copyright © 2016 Sadique_ebizon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
@interface SignUpViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameTxtFLd;

@property (weak, nonatomic) IBOutlet UITextField *emailTxtFld;

@property (weak, nonatomic) IBOutlet UITextField *employeeTxtFld;

@property (weak, nonatomic) IBOutlet UITextField *phoneNoTxtFld;



@end
