//
//  LoginViewController.h
//  EbizonInternalMarket
//
//  Created by Vivek_ebizon on 27/08/16.
//  Copyright © 2016 Sadique_ebizon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
@interface LoginViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UILabel *employeeIDLbl;
@property (weak, nonatomic) IBOutlet UIView *viewDetailsLbl;

@property (weak, nonatomic) IBOutlet UITextField *nameTxtFLd;
@property (weak, nonatomic) IBOutlet UILabel *phonenoLbl;


@property (weak, nonatomic) IBOutlet UITextField *emailTxtFld;



@end
