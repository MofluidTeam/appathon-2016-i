//
//  LoginViewController.m
//  EbizonInternalMarket
//
//  Created by Vivek_ebizon on 27/08/16.
//  Copyright © 2016 Sadique_ebizon. All rights reserved.
//

#import "LoginViewController.h"
#import "UploadPicViewController.h"
@interface LoginViewController ()

@end

@implementation LoginViewController
@synthesize nameTxtFLd,emailTxtFld;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)
nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
_viewDetailsLbl.hidden = YES;

    nameTxtFLd.delegate = self;
    emailTxtFld.delegate = self;

}
- (IBAction)logInActionBtton:(id)sender {
    
    
    NSArray *data = [[DBManager getSharedInstance]findByRegisterNumber:
                     emailTxtFld.text];
    if (data == nil) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:
                              @"Data not found" message:nil delegate:nil cancelButtonTitle:
                              @"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    else{
        NSString * email = emailTxtFld.text;
        _viewDetailsLbl.hidden = NO;
        _nameLbl.text = [data objectAtIndex:0];
        _emailLbl.text =email;
        emailTxtFld.hidden = YES;
        _employeeIDLbl.text = [data objectAtIndex:1];
        _phonenoLbl.text =[data objectAtIndex:2];
    }
}
    
    
    

- (IBAction)cancelBtton:(id)sender {
    
    
    _viewDetailsLbl.hidden = YES;
    
    UploadPicViewController *next = [self.storyboard instantiateViewControllerWithIdentifier:@"UploadPicViewController"];
    [self.navigationController pushViewController:next animated:YES];
    
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
