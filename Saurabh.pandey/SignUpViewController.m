//
//  SignUpViewController.m
//  EbizonInternalMarket
//
//  Created by Vivek_ebizon on 27/08/16.
//  Copyright © 2016 Sadique_ebizon. All rights reserved.
//

#import "SignUpViewController.h"
#import "LoginViewController.h"
#import "UploadPicViewController.h"
@interface SignUpViewController ()

@end

@implementation SignUpViewController
@synthesize nameTxtFLd , emailTxtFld ,phoneNoTxtFld ,employeeTxtFld;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)
nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    phoneNoTxtFld.delegate = self;
    employeeTxtFld.delegate = self;
    nameTxtFLd.delegate = self;
    emailTxtFld.delegate = self;
    // Do any additional setup after loading the view.
}
- (IBAction)signActionBtton:(id)sender {
    
//    NSString *name = nameTxtFLd.text;
//    NSString *email = emailTxtFld.text;
//    NSString *phoneNo = phoneNoTxtFld.text;
//    NSString *employeeID = employeeTxtFld.text;
    
    BOOL success = NO;
    NSString *alertString = @"Data Insertion failed";
    if (nameTxtFLd.text.length>0 &&emailTxtFld.text.length>0 &&
        employeeTxtFld.text.length>0 &&phoneNoTxtFld.text.length>0 )
    {
        success = [[DBManager getSharedInstance]saveData:
                   nameTxtFLd.text email:emailTxtFld.text employeeID:
                   employeeTxtFld.text mobileno:phoneNoTxtFld.text];
    }
    else{
        alertString = @"Enter all fields";
    }
    if (success == NO) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertString message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    if (success == YES) {
         NSString *alertString = @"Your details stored successfully in database.";
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertString message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        nameTxtFLd.text = @"";
        emailTxtFld.text= @"";
     phoneNoTxtFld.text= @"";
        employeeTxtFld.text = @"";
        
        
        UploadPicViewController *next = [self.storyboard instantiateViewControllerWithIdentifier:@"UploadPicViewController"];
        [self.navigationController pushViewController:next animated:YES];
        
        
    }

    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)logInActionBtton:(id)sender {
    
    LoginViewController *next = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:next animated:YES];
 
    
    
}










- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
