package com.employeebook.ebizon.employeebook;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by ebizon on 27/8/16.
 */
public class OptionListAdapter extends BaseAdapter {
    Activity activity;
    ArrayList<String> optionslist;
    LayoutInflater inflater;
    public OptionListAdapter(Activity activity, ArrayList<String> optionslist) {
        this.activity=activity;
        this.optionslist=optionslist;
        inflater=(LayoutInflater)activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return optionslist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=inflater.inflate(R.layout.optionlayout,null);
        TextView option=(TextView)view.findViewById(R.id.txtoption);
        option.setText(optionslist.get(position));
        return view;
    }
}
