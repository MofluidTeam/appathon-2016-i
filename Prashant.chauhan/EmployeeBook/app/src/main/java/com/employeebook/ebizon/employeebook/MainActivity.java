package com.employeebook.ebizon.employeebook;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.employeebook.ebizon.fragments.AddEmployeeFragment;
import com.employeebook.ebizon.fragments.DeleteEmployeeFragment;
import com.employeebook.ebizon.fragments.ViewEmployeeFragment;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> optionslist;
    private ListView optionListView;
    private Activity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
        getViewControls();
        optionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position)
                {
                    case 0:
                        optionListView.setVisibility(View.GONE);
                        AddEmployeeFragment addEmployeeFragment=new AddEmployeeFragment();
                        callFragment(addEmployeeFragment);
                        break;
                    case 1:
                        optionListView.setVisibility(View.GONE);
                        ViewEmployeeFragment viewEmployeeFragment=new ViewEmployeeFragment();
                        callFragment(viewEmployeeFragment);
                         break;
                    case 2:
                        optionListView.setVisibility(View.GONE);
                        DeleteEmployeeFragment deleteEmployeeFragment=new DeleteEmployeeFragment();
                        callFragment(deleteEmployeeFragment);
                        break;
                }

            }
        });
    }

    private void callFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

    }

    private void getViewControls() {
        optionListView=(ListView)findViewById(R.id.options_list);
        optionListView.setAdapter(new OptionListAdapter(activity,optionslist));

    }

    private void initialize() {
        activity=this;
        optionslist= new ArrayList<>();
        optionslist.add("Add Employee");
        optionslist.add("View Employee");
        optionslist.add("Remove Employee");
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack();
        super.onBackPressed();
    }
}
