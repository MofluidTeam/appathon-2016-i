package com.employeebook.ebizon.fragments;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.employeebook.ebizon.classes.EmployeDetails;
import com.employeebook.ebizon.employeebook.R;

import java.util.ArrayList;

/**
 * Created by ebizon on 27/8/16.
 */
public class EmployeeListAdapter extends BaseAdapter {
    Activity activity;
    ArrayList<EmployeDetails> employeedetailslist;
    LayoutInflater inflater;
    public EmployeeListAdapter(FragmentActivity activity, ArrayList employeelist) {
        this.activity=activity;
        this.employeedetailslist=employeelist;
        inflater=(LayoutInflater)activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return employeedetailslist.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view= inflater.inflate(R.layout.employelistrowlayout,null);
        TextView idview= (TextView) view.findViewById(R.id.id);
        TextView nameview= (TextView) view.findViewById(R.id.name);
        TextView departmentview= (TextView) view.findViewById(R.id.department);
        idview.setText(employeedetailslist.get(position).getCONTACTS_COLUMN_ID());
        nameview.setText(employeedetailslist.get(position).getCONTACTS_COLUMN_FIRSTNAME()+employeedetailslist.get(position).getCONTACTS_COLUMN_LASTNAME());
        departmentview.setText(employeedetailslist.get(position).getCONTACTS_COLUMN_DEPARTMENT());
        return view;
    }
}
