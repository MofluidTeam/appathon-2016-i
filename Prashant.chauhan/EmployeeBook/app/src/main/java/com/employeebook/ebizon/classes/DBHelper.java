package com.employeebook.ebizon.classes;

/**
 * Created by ebizon on 27/8/16.
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";
    public static final String CONTACTS_TABLE_NAME = "employee";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_FIRSTNAME = "firstname";
    public static final String CONTACTS_COLUMN_LASTNAME = "lastname";
    public static final String CONTACTS_COLUMN_EMAIL = "email";
    public static final String CONTACTS_COLUMN_JOBTYPE = "type";
    public static final String CONTACTS_COLUMN_DEPARTMENT = "department";
    public static final String CONTACTS_COLUMN_DESIGNATION = "designation";
    public static final String CONTACTS_COLUMN_SALARY= "salary";
    public static final String CONTACTS_COLUMN_HIREDATE= "hiredate";
    public static final String CONTACTS_COLUMN_PHONE = "phone";
    private HashMap hp;

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "CREATE TABLE IF NOT EXISTS " +
                        "("+CONTACTS_COLUMN_ID+" int IDENTITY(0,1) PRIMARY KEY, "+CONTACTS_COLUMN_FIRSTNAME+" varchar(255) NOT NULL, "+CONTACTS_COLUMN_LASTNAME+ " varchar(255) NOT NULL, "+CONTACTS_COLUMN_EMAIL+" varchar(255) NOT NULL, "+CONTACTS_COLUMN_JOBTYPE+" varchar(255) NOT NULL, "+CONTACTS_COLUMN_DEPARTMENT+" varchar(255) NOT NULL, "+CONTACTS_COLUMN_DESIGNATION+" varchar(255) NOT NULL, "+CONTACTS_COLUMN_SALARY+" varchar(255) NOT NULL, "+CONTACTS_COLUMN_HIREDATE+" varchar(255) NOT NULL, "+CONTACTS_COLUMN_PHONE+" varchar(255) NOT NULL)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS "+CONTACTS_TABLE_NAME);
        onCreate(db);
    }

    public boolean insertContact  (String firstname, String lastname, String email, String designation,String department,String hiredate,String mobileno,String salary,String type)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACTS_COLUMN_FIRSTNAME, firstname);
        contentValues.put(CONTACTS_COLUMN_LASTNAME, lastname);
        contentValues.put(CONTACTS_COLUMN_EMAIL, email);
        contentValues.put(CONTACTS_COLUMN_DEPARTMENT, department);
        contentValues.put(CONTACTS_COLUMN_DESIGNATION, designation);
        contentValues.put(CONTACTS_COLUMN_HIREDATE, hiredate);
        contentValues.put(CONTACTS_COLUMN_SALARY, salary);
        contentValues.put(CONTACTS_COLUMN_PHONE, mobileno);
        contentValues.put(CONTACTS_COLUMN_JOBTYPE, type);
        db.insert(CONTACTS_TABLE_NAME, null, contentValues);
        return true;
    }

    public Cursor getData(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+CONTACTS_TABLE_NAME +" wehere id ="+id, null );
        return res;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
        return numRows;
    }

   /* public boolean updateContact (Integer id, String name, String phone, String email, String street,String place)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("phone", phone);
        contentValues.put("email", email);
        contentValues.put("street", street);
        contentValues.put("place", place);
        db.update(CONTACTS_TABLE_NAME, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }*/

   /* public Integer deleteContact (Integer id)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(CONTACTS_TABLE_NAME,
                "id = ? ",
                new String[] { Integer.toString(id) });
    }*/

    public ArrayList<EmployeDetails> getAllCotacts()
    {
        ArrayList<EmployeDetails> array_list = new ArrayList<>();

        //hp = new HashMap();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+CONTACTS_TABLE_NAME, null );
        res.moveToFirst();
        while(res.isAfterLast() == false){
            EmployeDetails employeDetails=new EmployeDetails();
            employeDetails.setCONTACTS_COLUMN_ID(res.getString(res.getColumnIndex(CONTACTS_COLUMN_ID)));
            employeDetails.setCONTACTS_COLUMN_FIRSTNAME(res.getString(res.getColumnIndex(CONTACTS_COLUMN_FIRSTNAME)));
            employeDetails.setCONTACTS_COLUMN_LASTNAME(res.getString(res.getColumnIndex(CONTACTS_COLUMN_LASTNAME)));
            employeDetails.setCONTACTS_COLUMN_JOBTYPE(res.getString(res.getColumnIndex(CONTACTS_COLUMN_JOBTYPE)));
            employeDetails.setCONTACTS_COLUMN_DEPARTMENT(res.getString(res.getColumnIndex(CONTACTS_COLUMN_DEPARTMENT)));
            employeDetails.setCONTACTS_COLUMN_DESIGNATION(res.getString(res.getColumnIndex(CONTACTS_COLUMN_DESIGNATION)));
            employeDetails.setCONTACTS_COLUMN_HIREDATE(res.getString(res.getColumnIndex(CONTACTS_COLUMN_HIREDATE)));
            employeDetails.setCONTACTS_COLUMN_PHONE(res.getString(res.getColumnIndex(CONTACTS_COLUMN_PHONE)));
            employeDetails.setCONTACTS_COLUMN_SALARY(res.getString(res.getColumnIndex(CONTACTS_COLUMN_SALARY)));
            employeDetails.setCONTACTS_COLUMN_EMAIL(res.getString(res.getColumnIndex(CONTACTS_COLUMN_EMAIL)));
            array_list.add(employeDetails);
            res.moveToNext();
        }
        return array_list;
    }
}
