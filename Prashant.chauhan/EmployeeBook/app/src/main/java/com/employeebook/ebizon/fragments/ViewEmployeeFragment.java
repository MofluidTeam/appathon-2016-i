package com.employeebook.ebizon.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.employeebook.ebizon.classes.DBHelper;
import com.employeebook.ebizon.classes.EmployeDetails;
import com.employeebook.ebizon.employeebook.R;

import java.util.ArrayList;


public class ViewEmployeeFragment extends Fragment {

    private ListView employeelistview;
    private DBHelper dbHelper;
    private ArrayList<EmployeDetails> employeelist;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_view_employee, null);
        initialize();
        getViewControls(view);
        getListofEmployee();
        employeelistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                EmployeeDetailFragment employeeDetailFragment=new EmployeeDetailFragment(employeelist.get(position));
                callFragment(employeeDetailFragment);
            }
        });
        return view ;
    }


        private void callFragment(Fragment fragment) {
           getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();

        }

    private void initialize() {
        dbHelper=new DBHelper(getActivity());
        employeelist=new ArrayList();
    }

    private void getListofEmployee() {
        employeelist=dbHelper.getAllCotacts();
        if(employeelist.size()>0) {
            employeelistview.setAdapter(new EmployeeListAdapter(getActivity(), employeelist));
        }else{
            Toast.makeText(getActivity(), "No Data To Show", Toast.LENGTH_SHORT).show();
        }
    }

    private void getViewControls(View view) {
        employeelistview=(ListView)view.findViewById(R.id.employeelist);

    }


}
