package com.employeebook.ebizon.employeebook;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.employeebook.ebizon.classes.AllCommenMethod;
import com.employeebook.ebizon.classes.AllConstatntVariables;

import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;


    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private SharedPreferences sharedPreferences;
    private String savedUsername;
    private String savedPassword;
    private TextView createacoount;
    private EditText username;
    private EditText password;
    private  EditText confirmPassword;
    private LinearLayout signuplayout;
    private LinearLayout signinlayout;
    private TextView signinsignup;
    private Button signup;
    private Button signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
       /* hideActionBar();*/
        getDataFromSharedPreferences();
        if(savedUsername!=null&&savedPassword!=null)
        {
            popfragment();
            Intent intent=new Intent(this,MainActivity.class);
            startActivity(intent);
        }
        getViewControls();
        setListener();

    }

    private void popfragment() {
        FragmentManager fm = getSupportFragmentManager();
        fm.popBackStack();
    }

    private void getDataFromSharedPreferences() {
        sharedPreferences=getSharedPreferences(AllConstatntVariables.LOGINPREFERENCES,MODE_PRIVATE);
        savedUsername=sharedPreferences.getString(AllConstatntVariables.USERNAME,null);
        savedPassword=sharedPreferences.getString(AllConstatntVariables.PASSWORD,null);
    }

    private void hideActionBar() {
        getActionBar().hide();
    }

    private void setListener() {
       /* mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });*/
        createacoount.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                signuplayout.setVisibility(View.VISIBLE);
                signinlayout.setVisibility(View.GONE);
                signinsignup.setText("Sign Up");
            }
        });
        signup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setDataToPreferences();
            }
        });
        signin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

    }

    private void setDataToPreferences() {
        String usernamesignup=username.getText().toString();
        String passwordsignup=password.getText().toString();
        String confirmpasswordsignup=confirmPassword.getText().toString();

        boolean validation=validateAllFeilds(usernamesignup,passwordsignup,confirmpasswordsignup);

        if(validation) {
            if (passwordsignup.equals(confirmpasswordsignup)) {
                SharedPreferences.Editor editor = getSharedPreferences(AllConstatntVariables.LOGINPREFERENCES, MODE_PRIVATE).edit();
                editor.putString(AllConstatntVariables.USERNAME, usernamesignup);
                editor.putString(AllConstatntVariables.PASSWORD, passwordsignup);
                editor.commit();
                popfragment();
                Intent intent= new Intent(this,MainActivity.class);
                startActivity(intent);
            }
            else{
                Toast.makeText(this,"Password Mismatch",Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this,"Text Missing",Toast.LENGTH_SHORT).show();
        }

    }

    private boolean validateAllFeilds(String usernamesignup, String passwordsignup, String confirmpasswordsignup) {
        AllCommenMethod allCommenMetho=new AllCommenMethod();
        if(!allCommenMetho.verifynullString(usernamesignup)){
            return false;
        }else if(!allCommenMetho.verifynullString(passwordsignup)){
            return false;
        }else if(!allCommenMetho.verifynullString(passwordsignup))
            return false;
        else return true;
    }

    private void getViewControls() {
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();
        mPasswordView = (EditText) findViewById(R.id.password);
        signinlayout = (LinearLayout)findViewById(R.id.email_login_form);
        signuplayout = (LinearLayout)findViewById(R.id.signuplayout);
        mProgressView = findViewById(R.id.login_progress);
        createacoount=(TextView)findViewById(R.id.create_account);
        username=(EditText)findViewById(R.id.edtusername);
        password=(EditText)findViewById(R.id.edtpassword);
        confirmPassword=(EditText)findViewById(R.id.edtconfirmpassword);
        signinsignup=(TextView)findViewById(R.id.txtsignupsignin);
        signup=(Button)findViewById(R.id.signup);
        signin=(Button)findViewById(R.id.signin);
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        String mEmail = mEmailView.getText().toString().trim();
        String mPassword=mPasswordView.getText().toString().trim();
        AllCommenMethod allCommenMethod=new AllCommenMethod();
        boolean validate=allCommenMethod.verifynullString(mEmail);
        if(validate&&isPasswordValid(mPassword))
        {
           if(mEmail.equals(savedUsername)&&mPassword.equals(savedPassword))
           {
               popfragment();
               Intent intent=new Intent(this,MainActivity.class);
               startActivity(intent);
           }else{
               Toast.makeText(getApplicationContext(),"You are Not Registered Please Create an Account",Toast.LENGTH_SHORT).show();
           }
        }else{
            Toast.makeText(getApplicationContext(),"Please Enter Valid Username",Toast.LENGTH_SHORT).show();
        }
    }



    private boolean isPasswordValid(String password) {
      if(password!=null&&password.length()!=8&&password!="")
      {
          return true;
      }else{
          Toast.makeText(getApplicationContext(),"Please Enter Valid Password",Toast.LENGTH_SHORT).show();
          return false;
      }

    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


}

