package com.employeebook.ebizon.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.employeebook.ebizon.classes.AllCommenMethod;
import com.employeebook.ebizon.classes.DBHelper;
import com.employeebook.ebizon.employeebook.R;


public class AddEmployeeFragment extends Fragment {
    private EditText firstnameview;
    private EditText lastnameview;
    private EditText departmentview;
    private EditText designationview;
    private EditText emailview;
    private EditText salaryview;
    private EditText hiredateview;
    private EditText mobilenoview;
    private EditText typeview;
    private TextView submitview;
    private DBHelper dbHelper;
    private AllCommenMethod allCommenMethod;
    private String firstname;
    private String lastname;
    private String department;
    private String designation;
    private String salary;
    private String type;
    private String mobile;
    private String email;
    private String hiredate;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_base, container, false);
        initialize();
        getViewCOntrols(view);
        submitview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean validation=validatedata();
                if(validation)
                {
                    boolean status=dbHelper.insertContact(firstname,lastname,email,designation,department,hiredate,mobile,salary,type);
                    if(status)
                    {
                        Toast.makeText(getActivity(),"Your Information is saved",Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(getActivity(),"Something went wrong try again",Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(),"Invalid data",Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    private boolean validatedata() {
         firstname=firstnameview.getText().toString();
         lastname=lastnameview.getText().toString();
         department=departmentview.getText().toString();
         designation=designationview.getText().toString();
         salary=salaryview.getText().toString();
         type=typeview.getText().toString();
         mobile=mobilenoview.getText().toString();
         email=emailview.getText().toString();
         hiredate=hiredateview.getText().toString();
        if(!allCommenMethod.verifyLastName(firstname))
        {
           return false;
        }else   if(!allCommenMethod.verifynullString(lastname)){
            return false;
        }else   if(!allCommenMethod.verifynullString(department)){
            return false;
        }else   if(!allCommenMethod.verifynullString(designation)){
            return false;
        }else   if(!allCommenMethod.verifynullString(type)){
            return false;
        }else   if(!allCommenMethod.verifyEmail(email)){
            return false;
        }else   if(mobile.length()!=10){
            return false;
        }else   if(!allCommenMethod.verifynullString(hiredate)){
            return false;
        } else   if(!allCommenMethod.verifynullString(salary)){
            return false;
        }else return true;

    }

    private void initialize() {
        dbHelper=new DBHelper(getActivity());
        allCommenMethod=new AllCommenMethod();
    }

    private void getViewCOntrols(View view) {
        firstnameview=(EditText)view.findViewById(R.id.firstname);
        lastnameview=(EditText)view.findViewById(R.id.lastname);
        designationview=(EditText)view.findViewById(R.id.designation);
        departmentview=(EditText)view.findViewById(R.id.department);
        emailview=(EditText)view.findViewById(R.id.email);
        salaryview=(EditText)view.findViewById(R.id.salary);
        hiredateview=(EditText)view.findViewById(R.id.heiredate);
        mobilenoview=(EditText)view.findViewById(R.id.mobilenumber);
        typeview=(EditText)view.findViewById(R.id.type);
        submitview=(TextView)view.findViewById(R.id.submit);
    }


}
