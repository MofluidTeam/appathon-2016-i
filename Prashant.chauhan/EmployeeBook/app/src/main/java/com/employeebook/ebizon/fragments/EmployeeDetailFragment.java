package com.employeebook.ebizon.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.employeebook.ebizon.classes.AllCommenMethod;
import com.employeebook.ebizon.classes.DBHelper;
import com.employeebook.ebizon.classes.EmployeDetails;
import com.employeebook.ebizon.employeebook.R;


public class EmployeeDetailFragment extends Fragment {
    private EmployeDetails employeDetails;
    private TextView firstnameview;
    private TextView lastnameview;
    private TextView departmentview;
    private TextView designationview;
    private TextView emailview;
    private TextView salaryview;
    private TextView hiredateview;
    private TextView mobilenoview;
    private TextView typeview;
    private TextView ID;



    EmployeeDetailFragment(EmployeDetails employeDetails) {
        this.employeDetails=employeDetails;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_employeedetail, container, false);
        getViewControls(view);
        setDataToViews();
        return  view;
    }

    private void setDataToViews() {
        firstnameview.setText(employeDetails.getCONTACTS_COLUMN_FIRSTNAME());
        lastnameview.setText(employeDetails.getCONTACTS_COLUMN_LASTNAME());
        departmentview.setText(employeDetails.getCONTACTS_COLUMN_DEPARTMENT());
        designationview.setText(employeDetails.getCONTACTS_COLUMN_DESIGNATION());
        emailview.setText(employeDetails.getCONTACTS_COLUMN_EMAIL());
        ID.setText(employeDetails.getCONTACTS_COLUMN_ID());
        typeview.setText(employeDetails.getCONTACTS_COLUMN_JOBTYPE());
        salaryview.setText(employeDetails.getCONTACTS_COLUMN_SALARY());
        mobilenoview.setText(employeDetails.getCONTACTS_COLUMN_PHONE());
        hiredateview.setText(employeDetails.getCONTACTS_COLUMN_HIREDATE());

    }

    private void getViewControls(View view) {
        firstnameview=(TextView) view.findViewById(R.id.firstname);
        lastnameview=(TextView) view.findViewById(R.id.lastname);
        designationview=(TextView) view.findViewById(R.id.designation);
        departmentview=(TextView) view.findViewById(R.id.department);
        emailview=(TextView)view.findViewById(R.id.email);
        salaryview=(TextView)view.findViewById(R.id.salary);
        hiredateview=(TextView)view.findViewById(R.id.hiredate);
        mobilenoview=(TextView)view.findViewById(R.id.mobilenumber);
        typeview=(TextView)view.findViewById(R.id.type);
        ID=(TextView)view.findViewById(R.id.ID);

    }

}
