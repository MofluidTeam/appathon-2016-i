//
//  registration.swift
//  PRASHANT_SINGH_PS282
//
//  Created by Ebizon Netinfo Pvt Ltd on 27/08/16.
//  Copyright © 2016 kaleidosstudio. All rights reserved.
//

import UIKit

class registration: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var newTextField: UITextField!
    @IBOutlet weak var confirmPassTextField: UITextField!
    @IBOutlet weak var registorButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registorButton.layer.cornerRadius = 5
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
    }
    
    @IBAction func registorButton(sender: AnyObject) {
        
        let checkEmail = self.isValidEmail(self.emailTextField.text!)
        if(checkEmail == true){
            
            if(self.emailTextField.text != ""){
                
                if(self.newTextField.text?.characters.count >= 6){
                    
                    
                    
                    if(self.confirmPassTextField.text != "")
                    {
                        
                        
                        if(self.newTextField.text == self.confirmPassTextField.text)
                        {
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                            let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("Album") as! Album
                            self.navigationController?.pushViewController(nextViewController, animated: true)
                        }
                        else
                        {
                            let alert = UIAlertView()
                            alert.title = "Password"
                            alert.message = "Password Dose not match !"
                            alert.addButtonWithTitle("OK")
                            alert.show()
                            
                        }
                    
                    }
                    else
                    {
                        let alert = UIAlertView()
                        alert.title = "Password"
                        alert.message = "confirm Password is required !"
                        alert.addButtonWithTitle("OK")
                        alert.show()
                    }

                    
                }else{
                    let alert = UIAlertView()
                    alert.title = "Password"
                    alert.message = "Password should be at least 6 characters !"
                    alert.addButtonWithTitle("OK")
                    alert.show()
                }
            }
                
            else{
                let alert = UIAlertView()
                alert.title = "Password"
                alert.message = "Password is required !"
                alert.addButtonWithTitle("OK")
                alert.show()
            }
        }
            
        else{
            let alert = UIAlertView()
            alert.title = "Email"
            alert.message = "Email -ID is incorrect!"
            alert.addButtonWithTitle("OK")
            alert.show()
        }
    }

    
    func isValidEmail(email : String)->Bool{
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let testEmail = NSPredicate(format:"SELF MATCHES %@", regex)
        let status =  testEmail.evaluateWithObject(email)
        
        return status
    }

}
