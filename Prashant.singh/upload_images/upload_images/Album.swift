//
//  Album.swift
//  PRASHANT_SINGH_PS282
//
//  Created by Ebizon Netinfo Pvt Ltd on 27/08/16.
//  Copyright © 2016 kaleidosstudio. All rights reserved.
//

import UIKit

class Album: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var PhotoView: UIImageView!
    @IBOutlet weak var openCamera: UIButton!
    @IBOutlet weak var openLibray: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    

    
    
    override func viewDidLoad() {
    super.viewDidLoad()
    self.openCamera.layer.cornerRadius = 5
    self.openLibray.layer.cornerRadius = 5
    self.saveButton.layer.cornerRadius = 5
    self.shareButton.layer.cornerRadius = 5
    
    saveButton.hidden = true
    shareButton.hidden = true
    UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    @IBAction func openCamera(sender: AnyObject) {
        saveButton.hidden = false
        shareButton.hidden = false
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
            var imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
            imagePicker.allowsEditing = false
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func PhotoLibray(sender: AnyObject) {
        saveButton.hidden = false
        shareButton.hidden = false
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary) {
            var imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary;
            imagePicker.allowsEditing = true
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }

    }
    

    
func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
    PhotoView.image = image
    self.dismissViewControllerAnimated(true, completion: nil);
    }
    
    @IBAction func saveButton(sender: AnyObject) {
        var imageData = UIImageJPEGRepresentation(PhotoView.image!, 0.6)
        var compressedJPGImage = UIImage(data: imageData!)
        UIImageWriteToSavedPhotosAlbum(compressedJPGImage!, nil, nil, nil)
        
        var alert = UIAlertView(title: "Wow",
                                message: "Your image has been saved to Photo Library!",
                                delegate: nil,
                                cancelButtonTitle: "Ok")
        alert.show()
    }
    
    @IBAction func shareButton(sender: AnyObject) {
        // let image = UIImage(named: "logo")
        var imageData = UIImageJPEGRepresentation(PhotoView.image!, 0.6)
        var compressedJPGImage = UIImage(data: imageData!)
        
        
        // set up activity view controller
        let objectsToShare: [AnyObject] = [ compressedJPGImage! ]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        
        activityViewController.excludedActivityTypes = [
            UIActivityTypeAirDrop,
            UIActivityTypePostToFacebook,
            UIActivityTypePostToWeibo,
            UIActivityTypePrint,
            UIActivityTypeAssignToContact,
            UIActivityTypeSaveToCameraRoll,
            UIActivityTypeAddToReadingList,                                                        UIActivityTypePostToFlickr,                                                         UIActivityTypePostToVimeo]
        
        // present the view controller
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
}


