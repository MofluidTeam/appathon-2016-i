//
//  ViewController.swift
//  upload_images


import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    
    
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var login: UIButton!
    @IBOutlet weak var registor: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
     self.login.layer.cornerRadius = 5
     self.registor.layer.cornerRadius = 5
     UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    
    @IBAction func loginButoonAction(sender: AnyObject) {
        
            let checkEmail = self.isValidEmail(self.emailTextField.text!)
            if(checkEmail == true){
                
                if(self.passwordTextField.text != ""){
                    
                    if(self.passwordTextField.text?.characters.count >= 6){
                        
                        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
                        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("Album") as! Album
                        self.navigationController?.pushViewController(nextViewController, animated: true)
          
                        
                        //                            if self.loaderImageView != nil{
                        //                                view.addSubview(loaderImageView!)
                        //                            }
                        
                    }else{
                        let alert = UIAlertView()
                        alert.title = "Password"
                        alert.message = "Password should be at least 6 characters !"
                        alert.addButtonWithTitle("OK")
                        alert.show()
                    }
                }
                    
                else{
                    let alert = UIAlertView()
                    alert.title = "Password"
                    alert.message = "Password is required !"
                    alert.addButtonWithTitle("OK")
                    alert.show()
                }
            }
                
            else{
                let alert = UIAlertView()
                alert.title = "Email"
                alert.message = "Email -ID is incorrect!"
                alert.addButtonWithTitle("OK")
                alert.show()
            }
        }
    

    @IBAction func registor(sender: AnyObject) {
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewControllerWithIdentifier("reg") as! registration
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    
    func isValidEmail(email : String)->Bool{
        let regex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        let testEmail = NSPredicate(format:"SELF MATCHES %@", regex)
        let status =  testEmail.evaluateWithObject(email)
        
        return status
    }
    
    
}

