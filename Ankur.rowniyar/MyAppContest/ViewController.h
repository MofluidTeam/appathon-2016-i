//
//  ViewController.h
//  MyAppContest
//
//  Created by mac on 27/08/16.
//  Copyright © 2016 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *sareButtonOutlet;
@property (strong, nonatomic) IBOutlet UILabel *wlcmLbl;

- (IBAction)twitterBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *cameraImageView;
- (IBAction)cameraBtn:(id)sender;
- (IBAction)galeryBtn:(id)sender;
- (IBAction)sharBtn:(id)sender;

@end

